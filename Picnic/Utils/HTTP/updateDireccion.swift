//
//  updateDireccion.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
import Alamofire

func updateDireccionRequest(
    direccionId: String,
    nombreDireccion: String,
    numeroExterior: String,
    numeroInterior: String,
    referencias: String,
    completion: @escaping (updateDireccionModel?) -> Void){
    
    let ACTION_NAME = "updateDireccion"
    let URL = Constantes.SERVER_URL + Constantes.SERVER_PATH_USUARIO_GRUPOS + ACTION_NAME + "/" + direccionId
   
    let params : Parameters = [
        "nombreDireccion": nombreDireccion,
        "numeroExterior": numeroExterior,
        "numeroInterior": numeroInterior,
        "referencias": referencias
    ]

    AF.request(URL, method: .put, parameters: params, headers: Constantes.headersPicnic).responseJSON { response in
        do {
            let responseServer = try Utils.decoder.decode(updateDireccionModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
