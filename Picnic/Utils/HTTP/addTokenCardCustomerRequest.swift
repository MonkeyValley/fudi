//
//  addTokenCardCustomerRequest.swift
//  Picnic
//
//  Created by InnovacionVO on 24/02/21.
//  Copyright © 2021 Monkey Valley. All rights reserved.
//

import Alamofire

func addTokenCardCustomerRequest(
    openPayUserId: String,
    card_number: String,
    holder_name: String,
    expiration_year: String,
    expiration_month: String,
    cvv2: String,
    completion: @escaping (addTokenCardCustomerModel?) -> Void){
    
    let URL = Constantes.OPEN_PAY_URL + "/v1/" + Constantes.OPEN_PAY_ID + "/tokens";
    debugPrint(URL)
    let params : Parameters = [
        "card_number": card_number,
        "holder_name": holder_name,
        "expiration_year": expiration_year,
        "expiration_month": expiration_month,
        "cvv2": cvv2
    ]
    
    let payload = Payload(
        card_number: card_number,
        holder_name: holder_name,
        expiration_year: expiration_year,
        expiration_month: expiration_month,
        cvv2: cvv2
    )

    AF.request(
        URL,
        method: .post,
        parameters: payload,
        encoder: JSONParameterEncoder.default,
        headers: Constantes.headersOpenPay
    ).responseJSON { response in
        do {
            let responseServer = try Utils.decoder.decode(addTokenCardCustomerModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
