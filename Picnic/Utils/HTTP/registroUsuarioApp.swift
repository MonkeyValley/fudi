//
//  registroUsuarioApp.swift
//  Picnic
//
//  Created by ruben zamorano on 16/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Alamofire

func registroUsuarioAppRequest(
    cMail: String,
    cTelefono:String,
    password: String,
    cNombres: String,
    playerId: String,
    completion: @escaping (registroUsuarioAppModel?) -> Void){
    
    let ACTION_NAME = "registroUsuarioApp"
    let URL = Constantes.SERVER_URL + Constantes.SERVER_PATH_USUARIO_GRUPOS + ACTION_NAME
   
    let params : Parameters = [
        "cMail":cMail,
        "cTelefono":cTelefono,
        "password": password,
        "cNombres": cNombres,
        "playerId": playerId
    ]
    
    AF.request(URL, method: .post, parameters: params, headers: Constantes.headersPicnic).responseJSON { response in
        do {
            let responseServer = try Utils.decoder.decode(registroUsuarioAppModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
