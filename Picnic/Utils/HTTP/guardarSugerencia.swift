//
//  guardarSugerencia.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
import Alamofire

func guardarSugerenciaRequest(
    cNombreCliente: String,
    cNombreRestaurante: String,
    cTelefonoCliente: String,
    cTelefonoSucursal: String,
    nIdDireccion: String,
    nIdUsuario: String,
    regionEntrega: String,
    completion: @escaping (guardarSugerenciaModel?) -> Void){
    
    let ACTION_NAME = "guardarSugerencia"
    let URL = Constantes.SERVER_URL + Constantes.SERVER_PATH_USUARIO_GRUPOS + ACTION_NAME
   
    let params : Parameters = [
        "cNombreCliente": cNombreCliente,
        "cNombreRestaurante": cNombreRestaurante,
        "cTelefonoCliente": cTelefonoCliente,
        "cTelefonoSucursal": cTelefonoSucursal,
        "nIdDireccion": nIdDireccion,
        "nIdUsuario": nIdUsuario,
        "regionEntrega": regionEntrega
    ]

    AF.request(URL, method: .post, parameters: params, headers: Constantes.headersPicnic).responseJSON { response in
        do {
            let responseServer = try Utils.decoder.decode(guardarSugerenciaModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}

