//
//  addCardCustomer.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
import Alamofire

func addCardCustomerRequest(
    openPayUserId: String,
    card_number: String,
    holder_name: String,
    expiration_year: String,
    expiration_month: String,
    cvv2: String,
    completion: @escaping (addCardCustomerModel?) -> Void){
    
    let headersOpenPay: HTTPHeaders = [
            "Content-Type": "application/json",
        "Authorization": Utils.getAuthorizationOpenPay(data: Constantes.OPEN_PAY_SK)
            
        ]
    
    
    let URL = Constantes.OPEN_PAY_URL + "/v1/groups/" + Constantes.OPEN_PAY_GROUP_ID + "/customers/" + openPayUserId + "/cards";
    debugPrint(URL)
    
    let params : Parameters = [
        "card_number": card_number,
        "holder_name": holder_name,
        "expiration_year": expiration_year,
        "expiration_month": expiration_month,
        "cvv2": cvv2
    ]
    
    let payload = Payload(
        card_number: card_number,
        holder_name: holder_name,
        expiration_year: expiration_year,
        expiration_month: expiration_month,
        cvv2: cvv2
    )

    AF.request(
        URL,
        method: .post,
        parameters: payload,
        encoder: JSONParameterEncoder.default,
        headers: Constantes.headersOpenPay
    ).responseJSON { response in
        do {
            let responseServer = try Utils.decoder.decode(addCardCustomerModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}


struct Payload: Encodable {
    let card_number: String
    let holder_name: String
    let expiration_year: String
    let expiration_month: String
    let cvv2: String
}
