//
//  getCargaInicialRestaurante.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
import Alamofire

func getCargaInicialRestauranteRequest(
    nIdRestaurante: String,
    completion: @escaping (getCargaInicialRestauranteModel?) -> Void){
    
    let ACTION_NAME = "getCargaInicialRestaurante"
    let URL = Constantes.SERVER_URL + Constantes.SERVER_PATH_GENERAL_GRUPOS + ACTION_NAME + "/" + nIdRestaurante
   
    AF.request(URL, method: .get, headers: Constantes.headersPicnic).responseJSON { response in
        do {
            let responseServer = try Utils.decoder.decode(getCargaInicialRestauranteModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}

