//
//  guardarDireccion.swift
//  Picnic
//
//  Created by ruben zamorano on 16/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
import Alamofire

func guardarDireccionRequest(
    usuarioId: String,
    _lat: Double,
    _lng: Double,
    colonia: String,
    nombreCalle: String,
    nombreDireccion: String,
    numeroExterior: String,
    numeroInterior: String,
    referencias: String,
    tipoDirecccion: Int,
    completion: @escaping (guardarDirecciondModel?) -> Void){
    
    let ACTION_NAME = "guardarDireccion"
    let URL = Constantes.SERVER_URL + Constantes.SERVER_PATH_USUARIO_GRUPOS + ACTION_NAME + "/" + usuarioId
   
    let params : Parameters = [
        "_lat": _lat,
        "_lng": _lng,
        "colonia": colonia,
        "nombreCalle": nombreCalle,
        "nombreDireccion": nombreDireccion,
        "numeroExterior": numeroExterior,
        "numeroInterior": numeroInterior,
        "referencias": referencias,
        "tipoDirecccion": tipoDirecccion
    ]

    AF.request(URL, method: .post, parameters: params, headers: Constantes.headersPicnic).responseJSON { response in
        do {
            let responseServer = try Utils.decoder.decode(guardarDirecciondModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}


