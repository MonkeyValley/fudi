//
//  getPromociones.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Alamofire

func getPromocionesRequest(
    regionEntrega: String,
    completion: @escaping (getPromocionesModel?) -> Void){
    
    let ACTION_NAME = "getPromociones"
    let URL = Constantes.SERVER_URL + Constantes.SERVER_PATH_GENERAL_GRUPOS + ACTION_NAME + "/" + regionEntrega
   
    AF.request(URL, method: .get, headers: Constantes.headersPicnic).responseJSON { response in
        do {
            let responseServer = try Utils.decoder.decode(getPromocionesModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}

