//
//  agregarCupon.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
import Alamofire

func agregarCuponRequest(
    userId: String,
    cCodigoCupon: String,
    completion: @escaping (agregarCuponModel?) -> Void){
    
    let ACTION_NAME = "agregarCupon"
    let URL = Constantes.SERVER_URL + Constantes.SERVER_PATH_USUARIO_GRUPOS + ACTION_NAME + "/" + userId
   
    let params : Parameters = [
        "cCodigoCupon": cCodigoCupon
    ]

    AF.request(URL, method: .post, parameters: params, headers: Constantes.headersPicnic).responseJSON { response in
        do {
            let responseServer = try Utils.decoder.decode(agregarCuponModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
