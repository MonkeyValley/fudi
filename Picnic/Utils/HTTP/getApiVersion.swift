//
//  getApiVersion.swift
//  Picnic
//
//  Created by ruben zamorano on 15/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Alamofire

func getApiVersionRequest(completion: @escaping (getApiVersionModel?) -> Void){
    
    let ACTION_NAME = "getApiVersion"
    let URL = Constantes.SERVER_URL + Constantes.SERVER_PATH_GENERAL_GRUPOS + ACTION_NAME
   
    AF.request(URL,headers: Constantes.headersPicnic).responseJSON { response in
        do {
            let responseServer = try JSONDecoder().decode(getApiVersionModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
