//
//  RequestHelper.swift
//  Picnic
//
//  Created by ruben zamorano on 16/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation

enum requestHelper{
    
    static func getApiVersion(
        completion: @escaping (getApiVersionModel?) -> Void){
        getApiVersionRequest(completion: { result in
            completion(result)
        })
    }
    
    static func loginApp(
        cTelefono:String,
        password: String,
        playerId: String,
        completion: @escaping (loginAppModel?) -> Void){
        loginAppRequest(cTelefono: cTelefono, password: password, playerId: playerId, completion: { result in
            completion(result)
        })
    }
    
    static func registroUsuarioApp(
        cMail: String,
        cTelefono:String,
        password: String,
        cNombres: String,
        playerId: String,
        completion: @escaping (registroUsuarioAppModel?) -> Void){
        registroUsuarioAppRequest(
            cMail: cMail,
            cTelefono: cTelefono,
            password: password,
            cNombres: cNombres,
            playerId: playerId,
            completion: { result in
                completion(result)
            })
    }
    
    static func resetPassword(
        cMail:String,
        completion: @escaping (resetPasswordModel?) -> Void){
        resetPasswordRequest(cMail: cMail, completion: { result in
            completion(result)
        })
    }
    
    static func validateResetPassword(
        _id:String,
        code:String,
        newPassword:String,
        completion: @escaping (validateResetPasswordModel?) -> Void){
        validateResetPasswordRequest(
            _id: _id,
            code: code,
            newPassword: newPassword,
            completion: { result in
            completion(result)
        })
    }
    
    static func guardarDireccion(
        usuarioId: String,
        _lat: Double,
        _lng: Double,
        colonia: String,
        nombreCalle: String,
        nombreDireccion: String,
        numeroExterior: String,
        numeroInterior: String,
        referencias: String,
        tipoDirecccion: Int,
        completion: @escaping (guardarDirecciondModel?) -> Void){
        guardarDireccionRequest(
            usuarioId: usuarioId,
            _lat:_lat,
            _lng:_lng,
            colonia:colonia,
            nombreCalle:nombreCalle,
            nombreDireccion:nombreDireccion,
            numeroExterior:numeroExterior,
            numeroInterior:numeroInterior,
            referencias:referencias,
            tipoDirecccion:tipoDirecccion,
            completion: { result in
            completion(result)
        })
    }
    
    static func updateDireccion(
        direccionId: String,
        nombreDireccion: String,
        numeroExterior: String,
        numeroInterior: String,
        referencias: String,
        completion: @escaping (updateDireccionModel?) -> Void){
        updateDireccionRequest(
            direccionId: direccionId,
            nombreDireccion: nombreDireccion,
            numeroExterior: numeroExterior,
            numeroInterior: numeroInterior,
            referencias: referencias,
            completion: { result in
            completion(result)
        })
    }
    
    static func eliminarDireccion(
        direccionId: String,
        completion: @escaping (eliminarDireccionModel?) -> Void){
        eliminarDireccionRequest(
            direccionId: direccionId,
            completion: { result in
            completion(result)
        })
    }
    
    static func addCardCustomer(
        openPayUserId: String,
        card_number: String,
        holder_name: String,
        expiration_year: String,
        expiration_month: String,
        cvv2: String,
        completion: @escaping (addCardCustomerModel?) -> Void){
        addCardCustomerRequest(
            openPayUserId: openPayUserId,
            card_number: card_number,
            holder_name: holder_name,
            expiration_year: expiration_year,
            expiration_month: expiration_month,
            cvv2: cvv2,
            completion: { result in
            completion(result)
        })
    }
    static func addTokenCardCustomer(
            openPayUserId: String,
            card_number: String,
            holder_name: String,
            expiration_year: String,
            expiration_month: String,
            cvv2: String,
            completion: @escaping (addTokenCardCustomerModel?) -> Void){
            addTokenCardCustomerRequest(
                openPayUserId: openPayUserId,
                card_number: card_number,
                holder_name: holder_name,
                expiration_year: expiration_year,
                expiration_month: expiration_month,
                cvv2: cvv2,
                completion: { result in
                completion(result)
            })
    }
    
    static func getCardsCustomer(
        openPayUserId: String,
        completion: @escaping (getCardsCustomerModel?) -> Void){
        getCardsCustomerRequest(
            openPayUserId: openPayUserId,
            completion: { result in
            completion(result)
        })
    }
    
    static func getCupones(
        userId: String,
        completion: @escaping (getCuponesModel?) -> Void){
        getCuponesRequest(
            userId: userId,
            completion: { result in
            completion(result)
        })
    }
    
    static func agregarCupon(
        userId: String,
        cCodigoCupon: String,
        completion: @escaping (agregarCuponModel?) -> Void){
        agregarCuponRequest(
            userId: userId,
            cCodigoCupon: cCodigoCupon,
            completion: { result in
            completion(result)
        })
    }
    
    static func guardarSugerencia(
        cNombreCliente: String,
        cNombreRestaurante: String,
        cTelefonoCliente: String,
        cTelefonoSucursal: String,
        nIdDireccion: String,
        nIdUsuario: String,
        regionEntrega: String,
        completion: @escaping (guardarSugerenciaModel?) -> Void){
        guardarSugerenciaRequest(
            cNombreCliente: cNombreCliente,
            cNombreRestaurante: cNombreRestaurante,
            cTelefonoCliente: cTelefonoCliente,
            cTelefonoSucursal: cTelefonoSucursal,
            nIdDireccion: nIdDireccion,
            nIdUsuario: nIdUsuario,
            regionEntrega: regionEntrega,
            completion: { result in
            completion(result)
        })
    }
    
    static func guardarComentario(
        cTelefono: String,
        cMail: String,
        comentarios: String,
        lstTiposComentario: Array<DTO_Comentario>,
        completion: @escaping (guardarComentarioModel?) -> Void){
        saveComment(cTelefono: cTelefono, cMail: cMail, comentarios: comentarios, lstTiposComentario: lstTiposComentario, completion: {result in
            completion(result)
        })
    }
    
    static func getPedidosCliente(
        userId: String,
        completion: @escaping (getPedidosClienteModel?) -> Void){
        getPedidosClienteRequest(
            userId: userId,
            completion: { result in
            completion(result)
        })
    }
    
    
    static func getPromociones(
        regionEntrega: String,
        completion: @escaping (getPromocionesModel?) -> Void){
        getPromocionesRequest(
            regionEntrega: regionEntrega,
            completion: { result in
            completion(result)
        })
    }
    
    static func getSucursales(
        regionEntrega: String,
        completion: @escaping (getSucursalesModel?) -> Void){
        getSucursalesRequest(
            regionEntrega: regionEntrega,
            completion: { result in
            completion(result)
        })
    }
    
    
    static func getMD5(
        nIdRestaurante: String,
        completion: @escaping (getMD5Model?) -> Void){
        getMD5Request(
            nIdRestaurante: nIdRestaurante,
            completion: { result in
            completion(result)
        })
    }
    
    
    static func getCargaInicialRestaurante(
        nIdRestaurante: String,
        completion: @escaping (getCargaInicialRestauranteModel?) -> Void){
        getCargaInicialRestauranteRequest(
            nIdRestaurante: nIdRestaurante,
            completion: { result in
            completion(result)
        })
    }
    
    
    
    

    
    
    
    
    
}
