//
//  eliminarDireccion.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
import Alamofire

func eliminarDireccionRequest(
    direccionId: String,
    completion: @escaping (eliminarDireccionModel?) -> Void){
    
    let ACTION_NAME = "eliminarDireccion"
    let URL = Constantes.SERVER_URL + Constantes.SERVER_PATH_USUARIO_GRUPOS + ACTION_NAME + "/" + direccionId
   
    AF.request(URL, method: .delete, headers: Constantes.headersPicnic).responseJSON { response in
        do {
            let responseServer = try Utils.decoder.decode(eliminarDireccionModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}

