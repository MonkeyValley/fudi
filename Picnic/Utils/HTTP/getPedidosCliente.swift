//
//  getPedidos.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
import Alamofire

func getPedidosClienteRequest(
    userId: String,
    completion: @escaping (getPedidosClienteModel?) -> Void){
    
    let ACTION_NAME = "getPedidosCliente"
    let URL = Constantes.SERVER_URL + Constantes.SERVER_PATH_PEDIDOS_GRUPOS + ACTION_NAME + "/" + userId
   
    AF.request(URL, method: .get, headers: Constantes.headersPicnic).responseJSON { response in
        do {
            let responseServer = try Utils.decoder.decode(getPedidosClienteModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
