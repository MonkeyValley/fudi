//
//  resetPassword.swift
//  Picnic
//
//  Created by ruben zamorano on 16/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
import Alamofire

func resetPasswordRequest(
    cMail: String,
    completion: @escaping (resetPasswordModel?) -> Void){
    
    let ACTION_NAME = "resetPassword"
    let URL = Constantes.SERVER_URL + Constantes.SERVER_PATH_USUARIO_GRUPOS + ACTION_NAME
   
    let params : Parameters = [
        "cMail":cMail
    ]
    
    AF.request(URL, method: .post, parameters: params, headers: Constantes.headersPicnic).responseJSON { response in
        do {
            let responseServer = try Utils.decoder.decode(resetPasswordModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
