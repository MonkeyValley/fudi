//
//  getMD5.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
import Alamofire

func getMD5Request(
    nIdRestaurante: String,
    completion: @escaping (getMD5Model?) -> Void){
    
    let ACTION_NAME = "getMD5"
    let URL = Constantes.SERVER_URL + Constantes.SERVER_PATH_GENERAL_GRUPOS + ACTION_NAME + "/" + nIdRestaurante
   
    AF.request(URL, method: .get, headers: Constantes.headersPicnic).responseJSON { response in
        do {
            let responseServer = try Utils.decoder.decode(getMD5Model.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
