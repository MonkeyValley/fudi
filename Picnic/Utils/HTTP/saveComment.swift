//
//  saveComment.swift
//  Picnic
//
//  Created by InnovacionVO on 12/02/21.
//  Copyright © 2021 Monkey Valley. All rights reserved.
//
import Alamofire

func saveComment(
    cTelefono: String,
    cMail: String,
    comentarios: String,
    lstTiposComentario: Array<DTO_Comentario>,
    completion: @escaping (guardarComentarioModel?) -> Void){
    
    let ACTION_NAME = "guardarComentario"
    let URL = Constantes.SERVER_URL + Constantes.SERVER_PATH_USUARIO_GRUPOS + ACTION_NAME
   
    let params : Parameters = [
        "cTelefono": cTelefono,
        "cMail": cMail,
        "comentarios": comentarios,
        "lstTiposComentario": lstTiposComentario
    ]

    AF.request(URL, method: .post, parameters: params, headers: Constantes.headersPicnic).responseJSON { response in
        do {
            let responseServer = try Utils.decoder.decode(guardarComentarioModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}

