//
//  CustoAlert.swift
//  Picnic
//
//  Created by InnovacionVO on 06/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation
import UIKit


class CustomLoadding{

    struct Constants {
        static let backgroundAlphaTo:CGFloat = 0.4
    }
   
    private let backgroundView:UIView = {
        let background = UIView()
        background.backgroundColor = UIColor.quaternaryLabel
        return background
    }()
    
    private let alertView:UIView = {
        let alert = UIView()
        alert.backgroundColor = .secondarySystemBackground
        alert.layer.cornerRadius = 20.0
        alert.clipsToBounds = true
        return alert
    }()
    
    private var loaddingView:UIActivityIndicatorView?
    
    func showLoadding(on ViewController:UIViewController){
        guard let targetView = ViewController.view else {
            return
        }
        
        backgroundView.frame = targetView.bounds
        alertView.frame.origin = CGPoint(x: targetView.center.x - 50 , y: targetView.center.y - 50)
        alertView.frame.size = CGSize(width: 100.0, height: 100.0)
        
        loaddingView = UIActivityIndicatorView(style: .large)
        loaddingView!.frame.origin = CGPoint(x:  50 - (loaddingView!.frame.width/2), y: 50 - (loaddingView!.frame.width/2))
        loaddingView!.color = .label
        loaddingView!.startAnimating()
        
        alertView.addSubview(loaddingView!)
//        backgroundView.addSubview(alertView)
        
        targetView.addSubview(backgroundView)
        targetView.addSubview(alertView)
        
        UIView.animate(withDuration: 0.25) {
            self.backgroundView.alpha = Constants.backgroundAlphaTo
        }
    }

    
    func dismissAlert(){
                
        UIView.animate(withDuration: 0.25,animations:  {
            self.backgroundView.alpha = 0
        }, completion: { done in
            if(done){
                self.loaddingView!.removeFromSuperview()
                self.alertView.removeFromSuperview()
                self.backgroundView.removeFromSuperview()
            }
        })
    }
    
    /*func dismissAlertWhitResponse()-> String{
        
        var response = ""
        
        if(textView != nil){
            textView?.resignFirstResponder()
            response = (textView?.text!)!
            textView?.text = ""
        }
        
        alertView.alpha = 0
        
        UIView.animate(withDuration: 0.25,animations:  {
            self.backgroundView.alpha = 0
        }, completion: { done in
            if(done){
                self.alertView.removeFromSuperview()
                self.textView?.removeFromSuperview()
                self.backgroundView.removeFromSuperview()
                self.btn?.removeFromSuperview()
            }
        })
        
        return response
    }*/
}
