//
//  platilloModel.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

class platilloModel: Decodable {
    var _id: String?
    var bFoto: Bool?
    var bIngredienteElegir: Bool?
    var bPlatilloHome: Bool?
    var cDescripcion: String?
    var cFoto: String?
    var cNombrePlatillo: String?
    var comentarios: String?
    var nIdCategoria: String?
    var nIdRestaurante: String?
    var nIngredientes: Int?
    var nPrecio: Int?
    var quantity: Int?
    
    var lstIngredientes: [String]?
    var lstTamanios: [tamanioModel]?
}
