//
//  formaDePagoModel.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
class formaDePagoModel: Decodable {
    
    var device_session_id: String?
    var idTarjeta: String?
    var nTipoPago: Int?
    var pagaCon: Int?
}
