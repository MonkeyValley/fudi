//
//  addTokenCardCustomerModel.swift
//  Picnic
//
//  Created by InnovacionVO on 24/02/21.
//  Copyright © 2021 Monkey Valley. All rights reserved.
//
class addTokenCardCustomerModel: Decodable {
    
    //success
    var id: String?=""

    //error
    var http_code: Int?
    var error_code: Int?
    var category: String?
    var description: String?
    var request_id: String?
}
