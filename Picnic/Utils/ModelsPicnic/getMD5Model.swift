//
//  getMD5Model.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
class getMD5Model: Decodable {
    var bError: Bool?
    var cache: Bool?
    var apiVersion: String?
    var data: String?
}
