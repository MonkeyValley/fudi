//
//  complementoModel.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

class complementoModel: Decodable {
    var _id: String?
    var bComplementoPlatillo: Bool?
    var bDisponible: Bool?
    var cDescripcion: String?
    var nIdRestaurante: String?
    var nPrecio: Double?
    var quantity: Int?
}
