//
//  addCardCustomerModel.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

class addCardCustomerModel: Decodable {
    
    //success
    var id: String?=""
    var type: String?=""
    var brand: String?=""
    var address: String?=""
    var card_number: String?=""
    var holder_name: String?=""
    var expiration_year: String?=""
    var expiration_month: String?=""
    var allows_charges: Bool?
    var allows_payouts: Bool?
    var creation_date: String?=""
    var bank_name: String?=""
    var customer_id: String?=""
    var bank_code: String?=""

    //error
    var http_code: Int?
    var error_code: Int?
    var category: String?
    var description: String?
    var request_id: String?
}

