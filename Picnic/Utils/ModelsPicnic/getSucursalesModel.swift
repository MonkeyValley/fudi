//
//  getSucursalesModel.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
class getSucursalesModel: Decodable {
    var bError: Bool?
    var cache: Bool?
    var regionEntrega: String?
    var count: Int?
    var data: [sucursalModel]?
}
