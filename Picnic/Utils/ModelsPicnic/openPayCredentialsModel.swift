//
//  openPayCredentialsModel.swift
//  Picnic
//
//  Created by ruben zamorano on 16/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

class openpayCredentialsModel: Decodable {

    var id: String=""
    var name: String=""
    var last_name: String=""
    var phone_number: String=""
    var creation_date: String=""
    var email:String=""
    var address:String?
    var external_id:String?
    var clabe:String?
}

