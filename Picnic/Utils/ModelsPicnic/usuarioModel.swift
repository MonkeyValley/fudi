//
//  usuarioModel.swift
//  Picnic
//
//  Created by ruben zamorano on 16/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

class usuarioModel: Decodable {

    var _id: String = ""
    var cNombres: String = ""
    var cTelefono: String = ""
    var cMail: String = ""
    var playerId: String = ""
    var tipoUsuario: Int = 0
    var openPayCredentials: openpayCredentialsModel?
    var lstDirecciones: [direccionEntregaModel]?
}
