//
//  tamanioModel.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

class tamanioModel: Decodable {
    var bSeleccionado: Bool?
    var cDescripcion: String?
    var cNombreTamanio: String?
    var nPrecio:  Int?
    var quantity: Int?
}
