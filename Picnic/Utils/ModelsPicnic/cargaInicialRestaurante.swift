//
//  cargaInicialRestaurante.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
class cargaInicialRestaurante: Decodable {
    var _id: String?
    var nIdRestaurante: String?
    var lstBebidas: [bebidaModel]?
    var lstComplementos: [complementoModel]?
    var lstCategorias: [categoriaModel]?
    
}

