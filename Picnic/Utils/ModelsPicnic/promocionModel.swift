//
//  promocionModel.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
class promocionModel: Decodable {
    var _id: String?
    var bAccionBanner: Bool?
    var bRestriccionDia: Bool?
    var bRestriccionHora: Bool?
    var cDescripcion: String?
    var cFoto: String?
    var cNombrePromocion: String?
    var comentarios: String?
    var horaFin: Int?
    var horaInicio: Int?
    var minutoFin: Int?
    var minutoInicio: Int?
    var nIdCategoria: Int?
    var nIdRestaurante: String?
    var nPrecio: Int?
    var nombreRestaurante: String?
    var quantity: Int?
    var lstDiasPromocion: [diaPromocionModel]?
    
}
