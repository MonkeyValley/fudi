//
//  getApiVersion.swift
//  Picnic
//
//  Created by ruben zamorano on 16/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

class getApiVersionModel: Decodable {

    var API_VERSION: String

}
