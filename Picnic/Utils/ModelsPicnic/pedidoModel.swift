//
//  pedidoModel.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
class pedidoModel: Decodable {
    var index: Int?
    var bCompraInvitado: Bool?
    var bRecoger: Bool?
    var costoEnvio: Int?
    var fechaPedido: String?
    var nIdCategoria: Int?
    var nIdRestaurante: String?
    var nIdSucursal: String?
    var nIdUsuario: String?
    var nombreRestaurante: String?
    var nombreSucursal: String?
    var playerId: String?
    var regionEtrega: String?
    var statusPedido: Int?
    var subTotal: Double?
    var total: Double?
    
    var formaDePago: formaDePagoModel?
    var lstComplementos: [complementoModel]?
    var lstPlatillos: [platilloModel]?
    var lstPromociones: [promocionModel]?
    var lstBebidas: [bebidaModel]?
}
    
