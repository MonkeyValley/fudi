//
//  pedidoParentModel.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

class pedidoParentModel: Decodable {
    var _id: String=""
    var fechaPedido: String=""
    var grupo: String=""
    var nIdRestaurante: String=""
    var nIdSucursal: String=""
    var nIdUsuario: String=""
    var regionEntrega: String=""

    var nAnioPedido: Int=0
    var nDiaPedido: Int=0
    var nDiaSemanaPedido: Int=0
    var nFechaPedido: Int=0
    var nIdCategoria: Int=0
    var nMesPedido: Int=0
    var nSemanaPedido: Int=0
    var status: Int=0
    var tipoCompra: Int=0

    var bPedidoGuardado: Bool=false
    var bCompraPicnic: Bool=false
    var bPedidoInvitado: Bool=false
    
    var pedido: pedidoModel?
}
