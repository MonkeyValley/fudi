//
//  categoriaModel.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

class categoriaModel: Decodable {
    var _id: String?
    var bActivo: Bool?
    var cDescripcion: String?
    var nIdRestaurante: String?
    var nOrden: Int?
    var lstPlatillos: [platilloModel]?
    
}
