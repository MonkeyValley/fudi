//
//  direccionEntregaModel.swift
//  Picnic
//
//  Created by ruben zamorano on 16/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//


class direccionEntregaModel: Decodable {

    var _id: String=""
    var nombreDireccion: String=""
    var nombreCalle: String=""
    var numeroExterior: String=""
    var numeroInterior: String=""
    var referencias: String=""
    var colonia: String=""
    var tipoDirecccion: Int=0
    var Usuario: String=""
    var regionEntrega: String=""

    var _lat: Double=0
    var _lng: Double=0
}

