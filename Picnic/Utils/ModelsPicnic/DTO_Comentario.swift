//
//  DTO_Comentario.swift
//  Picnic
//
//  Created by InnovacionVO on 12/02/21.
//  Copyright © 2021 Monkey Valley. All rights reserved.
//

class DTO_Comentario: Decodable {
    var tipoComentario: Int=0
    var descripcionComentario: String=""
}
