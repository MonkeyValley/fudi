//
//  cuponModel.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

class cuponModel: Decodable {
    
    var _id: String=""
    var nIdUsuario: String=""
    var nIdCupon: String=""
    var bCanjeado: Bool=false
    var cCodigoCupon: String=""
    var cDescripcion: String=""
    var cNombreRestaurante: String=""
    var nIdRestaurante: String=""
    var nTipoCupon: Int=0
    var nValorCupon:  Int=0
}
