//
//  sucursalModel.swift
//  Picnic
//
//  Created by ruben zamorano on 17/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
class sucursalModel: Decodable {
    var _id: String?
    var bActivo: Bool?
    var cPassword: String?
    var cLogin: String?
    var dHoraCierre: String?
    var dHoraApertura: String?
    var cDireccion: String?
    var nTiempoEntrega: Int?
    var costoEnvio: Int?
    var cNombreSucursal: String?
    var nLatitud: Double?
    var nLongitud: Double?
    var nCostoEntrega: Int?
    var nTiempoEspera: Int?
    var nIdRestaurante: String?
    var TIME_ZONE: String?
    var nHoraApertura: Int?
    var nHoraCierre: Int?
    var nMinutoApertura: Int?
    var nMinutoCierre: Int?
    var regionEntrega: String?
    var bSoloRecoger: Bool?
    var cTelefono: String?
    var tipoEntrega: Int?
    var cNombreRestaurante: String?
    var nIdCategoria: Int?
    var cFoto: String?
    var nIdFormaPago: Int?
    var pedidoMinimo: Int?
    var nTipoDescuento: Int?
    var descuentoPicnic: Int?
    
    var lstRangoEntrega: [locationModel]?
    var diasDescuentoPicnic: [diaPromocionModel]?
}
