//
//  Constantes.swift
//  Picnic
//
//  Created by ruben zamorano on 16/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//
import Alamofire
enum Constantes {
    
    static let API_VERSION = "1";

    static let OPEN_PAY_ID = "m15n5vofm874uvb5btkq";
    static let OPEN_PAY_SK = "sk_09917cdfeb734b4eb5e8e341959bba33";
    static let OPEN_PAY_PK = "pk_c6632490755d489e8364bfecbcde2d27";

    static let OPEN_PAY_GROUP_ID = "gkkag8lxw6zzzrypjfsf";
    static let OPEN_PAY_SECRET_KEY = "sk_4d98840e559a4f2aa002159192e561d2";
    
    static let OPEN_PAY_URL = "https://sandbox-api.openpay.mx";
    static let OPEN_PAY_PRODUCTION_MODE = false;
    static let SERVER_URL = "https://tritonsoft.tech";


    static let SERVER_PATH_GENERAL_GRUPOS = "/picnic-api/api/grupos/general/";
    static let SERVER_PATH_USUARIO_GRUPOS = "/picnic-api/api/grupos/usuarios/";
    static let SERVER_PATH_PEDIDOS_GRUPOS = "/picnic-api/api/grupos/pedidos/";

    static let nIdRestaurante = "5ed54bd60822502d2c9b0938";
    static let GRUPO_ID = "5f2c9a0b7a9be9acda3b3d68";

    static let PEDIDO_EN_SUCURSAL = 0;
    static let PEDIDO_EN_PREPARACION = 1;
    static let PEDIDO_EN_CAMINO = 2;
    static let PEDIDO_ENTREGADO = 3;
    static let PEDIDO_CANCELADO = 4;
    static let PEDIDO_RECOGER_LISTO = 5;

    static let PLATILLO = 1;
    static let COMPLEMENTO = 2;
    static let BEBIDA = 3;
    static let PROMOCION = 4;

    static let TAB_HOME = 0;
    static let TAB_BUSCAR = 1;
    static let TAB_HISTORIAL = 2;
    static let TAB_PERFIL = 3;

    static let CATEGORIA_PROMOCIONES = 0;
    static let CATEGORIA_SUSHI = 1;
    static let CATEGORIA_HAMBURGUESAS = 2;
    static let CATEGORIA_CHINA = 3;
    static let CATEGORIA_TACOS = 4;
    static let CATEGORIA_MARISCOS = 5;
    static let CATEGORIA_PIZZA = 6;
    static let CATEGORIA_MEXICANA = 7;
    static let CATEGORIA_CORTES = 8;
    static let CATEGORIA_ENSALADAS = 9;
    static let CATEGORIA_PASTELES = 10;


    static let PAGO_TARJETA_EFECTIVO = 0;
    static let PAGO_SOLO_EFECTIVO = 1;
    static let PAGO_SOLO_TARJETA = 2;

    static let ENTREGA_NORMAL = 1;
    static let ENTREGA_SOLO_DOMICILIO = 2;
    static let ENTREGA_SOLO_RECOGER = 3;


    static let DESCUENTO_GENERAL = 1;
    static let DESCUENTO_EMPLEADO = 2;

    static let USUARIO_PICNIC = 1;
    static let USUARIO_CORPORATIVO = 2;

    static let TIPO_CUPON_DESCUENTO_PORCENTAJE = 1;
    static let TIPO_CUPON_DESCUENTO_EFECTIVO = 2;
    static let TIPO_CUPON_ENVIO_GRATIS = 3;
    static let TIPO_CUPON_2X1 = 4;
    static let TIPO_CUPON_PRODUCTO_GRATIS = 5;


    static let TIPO_COMPRA_NORMAL = 1;
    static let TIPO_COMPRA_DESCUENTO_ESPECIAL = 2;
    static let TIPO_COMPRA_CUPON = 3;
    
    static let IMAGE_BASE = "https://drive.google.com/uc?export=download&id="

    static let headersPicnic: HTTPHeaders = [
        "picnic-group": GRUPO_ID
    ]
    
    static let headersOpenPay: HTTPHeaders = [
        "Content-Type": "application/json",
        "Authorization": Utils.getAuthorizationOpenPay(data: OPEN_PAY_SECRET_KEY)
        
    ]
}
