//
//  ToastTop.swift
//  Picnic
//
//  Created by InnovacionVO on 24/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation
import UIKit

class Toast {
    
    
    static func showAlert(with message:String, on ViewController:UIViewController, time:String){
            
        guard let targetView = ViewController.view else {
            return
        }
        
        
        
        let alertView:UIView = {
            let alert = UIView()
            alert.backgroundColor = .systemFill
            alert.layer.masksToBounds = true
            return alert
        }()
        
        let title:UILabel = {
            let title = UILabel()
            title.font = UIFont(name: "Helvetica-SemiBold", size: 14.0)
            title.textColor = UIColor.darkGray
            return title
        }()
        
        title.text = message
        var timeLong:Double?
        
        alertView.cornerRadius = 15.0

        if time == "short" {
            timeLong = 1.5
        }else{
            timeLong = 3.0
        }
        
        
        alertView.frame = CGRect(x: 16 , y: 0, width: targetView.frame.size.width - 32, height: 55)
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.systemMaterial)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = alertView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        title.frame = CGRect(x: 8, y: 3, width: alertView.frame.width - 16.0, height: alertView.frame.height)
        
        alertView.addSubview(blurEffectView)
        alertView.addSubview(title)

        UIView.animate(withDuration: 0.25,animations:  {
            alertView.frame = CGRect(x: 16.0, y: 20.0, width: (Double(targetView.frame.size.width) - 32.0), height: 55.0)
        }, completion: { done in
            
            alertView.frame = CGRect(x: 16 , y: 20.0 , width: Double(targetView.frame.size.width - 32), height: 55)
            print(timeLong!)
            
            _ = Timer.scheduledTimer(withTimeInterval: timeLong!, repeats: false){ timer in
                UIView.animate(withDuration: 0.25,animations:  {
                    alertView.frame = CGRect(x: 16 , y: -55.0, width: Double(targetView.frame.size.width - 32) , height: 55.0)
                }, completion: { done in
                    if(done){
                        alertView.removeFromSuperview()
                        timeLong! = 0.0
                    }
                })
            }
        })
        
        targetView.addSubview(alertView)
    }
    
}
