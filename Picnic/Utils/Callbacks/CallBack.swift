//
//  CallBack.swift
//  Picnic
//
//  Created by InnovacionVO on 01/03/21.
//  Copyright © 2021 Monkey Valley. All rights reserved.
//

import Foundation


protocol onCuponSuccess {
    func onClose(response:DiscountsModelRealm)
}
