//
//  UtilsFunction.swift
//  Picnic
//
//  Created by InnovacionVO on 06/11/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation
import CoreData

class utils {
    
     func ifExist(id:Int) -> Bool {
           var req = false
           
           let fetchReq:NSFetchRequest<ShoppingCart> = ShoppingCart.fetchRequest()
           
           do{
              let item = try PersistenceService.context.fetch(fetchReq)
               
               item.forEach { it in
                   if(id == it.id){
                       req = true
                   }
               }
               
           }catch{
               req = false
           }
           
           return req
       }
}
