//
//  Utils.swift
//  Picnic
//
//  Created by ruben zamorano on 16/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation
import UIKit
enum Utils {
    static let decoder = JSONDecoder()
    
    static func fromBase64(data: String) -> String? {
        guard let result = Data(base64Encoded: data) else {
            return nil
        }

        return String(data: result, encoding: .utf8)
    }

    static func toBase64(data: String) -> String {
        return Data(data.utf8).base64EncodedString()
    }
    
    static func getAuthorizationOpenPay(data: String) -> String {
        return "Basic " + toBase64(data: data + ":" + "")
    }
    
    static func getEoticonsfromCategory(idCategory:Int)-> String{
        
        var rqeust = ""
        
        switch idCategory {
        case 0:
            rqeust = "📢🔥"
            break
        case 1:
            rqeust = "🍣🍙"
            break
        case 2:
            rqeust = "🍔🌭"
            break
        case 3:
            rqeust = "🥡🍜"
            break
        case 4:
            rqeust = "🌮🥑"
            break
        case 5:
            rqeust =  "🦐🐟"
            break
        case 6:
            rqeust = "🍕🍝"
            break
        case 7:
            rqeust = "🇲🇽🌶"
            break
        case 8:
            rqeust = "🥩🍖"
            break
        case 9:
            rqeust = "🥬🥗"
            break
        case 10:
            rqeust = "🎂🍩"
            break
        default:
            break
        }
        
        return rqeust
        
    }
    
    static func getImagefromCategory(idCategory:Int) -> String{
        
        var request = ""
        switch idCategory {
        case 0:
            request = "cat_promo"
            break
        case 1:
            request = "cat_sushi"
            break
        case 2:
            request =  "cat_hamburguesas"
            break
        case 3:
            request =  "cat_china"
            break
        case 4:
            request =  "cat_tacos"
            break
        case 5:
            request =  "cat_mariscos"
            break
        case 6:
            request =  "cat_pizza"
            break
        case 7:
            request =  "cat_mexicana"
            break
        case 8:
            request =  "cat_cortes"
            break
        case 9:
            request =  "cat_ensalada"
            break
        case 10:
            request =  "cat_pasteles"
            break
        default:
            break
        }
        return request
    }
    
    static func getStatusHistory(stauts:Int) -> String {
        var result = ""
        switch stauts {
        case 0:
            result = "Pendiente"
        case 1:
            result = "En Preparación"
        case 2:
            result = "En camino"
        case 3:
            result = "Entregado"
        case 4:
            result = "Cancelado"
        case 5:
            result = "Listo para recoger"
        default:
            result = "Error"
        }
        
        return result
    }
    
    static func getColorFromNumStatus(stauts:Int) -> UIColor {
        
        var result = UIColor.lightGray
        switch stauts {
        case 1:
            result = UIColor.orange
        case 2:
            result = UIColor(hex: "#1e90ffff")!
        case 3, 5:
            result = UIColor(hex: "#2e7d32ff")!
        case 4:
            result = UIColor.red
        default:
            result = UIColor.lightGray
        }
        
        return result
        
    }
    static func getMonthFromNumMonth(month:Int) -> String {
        var result = ""
        switch month {
        case 0:
            result = "Ene"
        case 1:
            result = "Feb"
        case 2:
            result = "Mar"
        case 3:
            result = "Abr"
        case 4:
            result = "May"
        case 5:
            result = "Jun"
        case 6:
            result = "Jul"
        case 7:
            result = "Ago"
        case 8:
            result = "Sep"
        case 9:
            result = "Oct"
        case 10:
            result = "Nov"
        case 11:
            result = "Dic"
        default:
            result = "Sin fecha"
        }
        
        return result
    }
    
    static func formatDay2Nubs(numb:Int)-> String {
        if(numb < 10){
            return "0" + String(numb)
        }else{
            return "\(numb)"
        }
    }
    
}
extension UIColor {
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat

        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])

            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0

                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255

                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }

        return nil
    }
}
