//
//  CustoAlert.swift
//  Picnic
//
//  Created by InnovacionVO on 06/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation
import UIKit


class CustoAlert{
    
    struct Constants {
        static let backgroundAlphaTo:CGFloat = 0.6
    }
   
    var delegatexyz:AddNoteDelegate?
    private let backgroundView:UIView = {
        let background = UIView()
        background.backgroundColor = .secondarySystemBackground
        background.alpha = 0.0
        return background
    }()
    
    private let alertView:UIView = {
        let alert = UIView()
        alert.backgroundColor = .systemBackground
        alert.layer.masksToBounds = true
        return alert
    }()
    
    private var textView:UITextField?
    private var btn:UIButton?
    private var note:UITextField?

    
    func showBottomSheet(whit tilte:String, messagge:String, on ctx:UIViewController, delegate addnote:AddNoteDelegate){
        guard let targetView = ctx.view else {
            return
        }
        
        delegatexyz = addnote
        
        let widthBackgroundView = targetView.frame.width
        let heightBackgroundView = targetView.frame.height
        
        let heightAlert = 300
        
        let originYbutton = 10
        let originYimage = 40
        let originYviewcoments = 20
        let originYaddButton = 10
        let heighImage = 60
        let heightCloseButton = 32
        
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.endEditing)))
        backgroundView.backgroundColor = .clear
        backgroundView.frame.size = targetView.frame.size
        backgroundView.frame.origin = CGPoint(x: 0, y: targetView.frame.height)
        
        alertView.backgroundColor = .secondarySystemBackground
        alertView.alpha = 1.0
        alertView.frame.size = CGSize(width: CGFloat(widthBackgroundView) - 16.0 , height: CGFloat(heightAlert))
        alertView.frame.origin = CGPoint(x: 8, y: heightBackgroundView - 316 )
        alertView.layer.cornerRadius = 15.0
        
        
        let image = UIImageView(frame: CGRect(x: 10, y: originYimage, width: heighImage, height: heighImage))
        image.backgroundColor = .tertiarySystemBackground
        image.layer.cornerRadius = 30.0
        image.image = UIImage(systemName: "note.text")
        image.contentMode = .center
        image.tintColor = .white
        
        let button = UIButton(frame:  CGRect(x: alertView.frame.width - 48, y: CGFloat(originYbutton), width: CGFloat(heightCloseButton), height: CGFloat(heightCloseButton)))
        button.backgroundColor = UIColor.init(red: 238/255, green: 238/255, blue: 238/255, alpha: 1.0)
        button.layer.cornerRadius = 16.0
        button.setImage( UIImage(systemName: "xmark") , for: .normal)
        button.tintColor = .black
        button.addTarget(self, action: #selector(self.dissmisBottomSheet), for: .touchUpInside)
        
        
        let viewNote = UIView(frame: CGRect(x: 8, y: CGFloat(originYimage + heighImage + originYviewcoments), width: alertView.frame.width - 16, height: 45))
        viewNote.backgroundColor = .tertiarySystemBackground
        viewNote.layer.cornerRadius = 10.0
        viewNote.clipsToBounds = true
        
        self.note = UITextField(frame: CGRect(x: 8, y: 0, width: viewNote.frame.width - 8 , height: 45) )
        self.note!.placeholder = "Agrega tu nota"
        
        self.note!.addTarget(self, action: #selector(self.textFieldDidBegginEdit(_:)), for: .editingDidBegin)
        self.note!.addTarget(self, action: #selector(self.textFieldDidEndEdit(_:)), for: .editingDidEnd)
        
        let buttonAdd = UIButton(frame:  CGRect(x: 8, y: CGFloat(originYimage + heighImage + originYviewcoments + originYaddButton + 45), width: alertView.frame.width - 16, height: 45))
        buttonAdd.layer.cornerRadius = 10
        buttonAdd.setTitle("Agregar", for: .normal)
        buttonAdd.backgroundColor = UIColor(red: 254/255, green: 70/255, blue: 0/255, alpha: 1.0)
        buttonAdd.addTarget(self, action: #selector(self.sendNote), for: .touchUpInside)
        
        viewNote.addSubview(self.note!)
        alertView.addSubview(buttonAdd)
        alertView.addSubview(viewNote)
        alertView.addSubview(image)
        alertView.addSubview(button)
        backgroundView.addSubview(alertView)
        targetView.addSubview(backgroundView)
        
        
        
        UIView.animate(withDuration: 0.25,animations: {
            self.backgroundView.alpha = 1.0
            self.backgroundView.frame.origin = CGPoint(x: 0, y: 0)
        }, completion: {_ in
            self.backgroundView.frame.origin = CGPoint(x: 0, y: 0)
        })

    }
    
    @objc func sendNote(){
        self.dissmisBottomSheet()
        self.delegatexyz!.xyz(note: note!.text!)
    }
    
    @objc func endEditing(){
        self.note!.resignFirstResponder()
    }
    @objc func dissmisBottomSheet(){
        UIView.animate(withDuration: 0.25,animations: {
            self.backgroundView.frame.origin = CGPoint(x: 0, y: self.backgroundView.frame.height)
        }, completion: {_ in
            self.backgroundView.frame.origin = CGPoint(x: 0, y: self.backgroundView.frame.height)
            self.backgroundView.removeFromSuperview()
        })
    }
    
    @objc func textFieldDidBegginEdit(_ textField: UITextField) {
        UIView.animate(withDuration: 0.25,animations: {
            self.alertView.frame.origin = CGPoint(x: 8, y: self.backgroundView.frame.height - self.alertView.frame.height - 200 )
        }, completion: {_ in
            self.alertView.frame.origin = CGPoint(x: 8, y: self.backgroundView.frame.height - self.alertView.frame.height - 200 )
        })
    }
    @objc func textFieldDidEndEdit(_ textField: UITextField) {
        UIView.animate(withDuration: 0.25,animations: {
            self.alertView.frame.origin = CGPoint(x: 8, y: self.backgroundView.frame.height - 316 )
        }, completion: {_ in
            self.alertView.frame.origin = CGPoint(x: 8, y: self.backgroundView.frame.height - 316 )
        })
    }
    func showAlert(with hint:String, type:String, on ViewController:UIViewController){
        guard let targetView = ViewController.view else {
            return
        }
        backgroundView.frame = targetView.bounds
        alertView.alpha = 1
        switch type {
        case "edittext":
            alertView.frame = CGRect(x: 0 , y: (targetView.frame.size.height/2), width: targetView.frame.size.width, height: 55)
            
            let icon = UIImageView(frame: CGRect(x: 0, y: 7.5 , width: 40, height: 40))
            icon.image = #imageLiteral(resourceName: "ic_comment")
            icon.contentMode = .center
            
            textView = UITextField(frame: CGRect(x: 48, y: 8, width: targetView.frame.size.width - 93, height: 40))
            textView!.placeholder = hint
            textView!.returnKeyType = .done
            textView?.becomeFirstResponder()
            
            
            btn = UIButton(frame: CGRect(x: textView!.frame.width + 43 , y: 8, width: 40, height: 40))
            btn!.cornerRadius = 5

            
            btn!.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
            btn!.setTitle("OK", for: .normal)
            btn!.setTitleColor(.white, for: .normal)
            
            
            
            alertView.addSubview(btn!)
            alertView.addSubview(icon)
            alertView.addSubview(textView!)
            
        default:
            alertView.frame = CGRect(x: (targetView.frame.size.width/2) - ((targetView.frame.size.width - 80)/2), y: (targetView.frame.size.height/2) - 175, width: (targetView.frame.size.width - 80), height: 350)
            alertView.cornerRadius = 12
            
        }
        
        targetView.addSubview(backgroundView)
        targetView.addSubview(alertView)
        
        UIView.animate(withDuration: 0.25) {
            self.backgroundView.alpha = Constants.backgroundAlphaTo
        }
    }

    
    func dismissAlert(){
        if(textView != nil){
            textView?.resignFirstResponder()
        }
        
        alertView.alpha = 0
        for view in alertView.subviews{
            view.removeFromSuperview()
        }
        UIView.animate(withDuration: 0.25,animations:  {
            self.backgroundView.alpha = 0
        }, completion: { done in
            if(done){
                self.alertView.removeFromSuperview()
                self.backgroundView.removeFromSuperview()
            }
        })
    }
    @objc func dismissAlertObjc(){
        if(textView != nil){
            textView?.resignFirstResponder()
        }
        
        alertView.alpha = 0
        for view in alertView.subviews{
            view.removeFromSuperview()
        }
        UIView.animate(withDuration: 0.25,animations:  {
            self.backgroundView.alpha = 0
        }, completion: { done in
            if(done){
                self.alertView.removeFromSuperview()
                self.backgroundView.removeFromSuperview()
            }
        })
    }
    
    func dismissAlertWhitResponse()-> String{
        
        var response = ""
        
        if(textView != nil){
            textView?.resignFirstResponder()
            response = (textView?.text!)!
            textView?.text = ""
        }
        
        alertView.alpha = 0
        
        UIView.animate(withDuration: 0.25,animations:  {
            self.backgroundView.alpha = 0
        }, completion: { done in
            if(done){
                self.alertView.removeFromSuperview()
                self.textView?.removeFromSuperview()
                self.backgroundView.removeFromSuperview()
                self.btn?.removeFromSuperview()
            }
        })
        
        return response
    }
    
    func showAlertInfo(title:String, description:String, buttonTextLeft:String, buttonTextRight:String, type:String, on ViewController:UIViewController, leftButton:UIButton?, rightButton:UIButton){
        guard let targetView = ViewController.view else {
            return
        }
        
        backgroundView.frame = targetView.bounds
        alertView.alpha = 1
        let cancelableAction = UITapGestureRecognizer(target: self, action: #selector(dismissAlertObjc))
        backgroundView.addGestureRecognizer(cancelableAction)
        alertView.frame = CGRect(x: (targetView.frame.size.width/2) - ((targetView.frame.size.width - 80)/2), y: (targetView.frame.size.height/2) - 175, width: (targetView.frame.size.width - 80), height: 350)
        alertView.cornerRadius = 12
              

        
            let imageAlert = UIImageView(frame: CGRect(x: (alertView.frame.size.width / 2) - 50, y: 28.0, width: 100.0, height: 100.0))
            let lblTitle = UILabel(frame: CGRect(x:16.0, y: imageAlert.frame.height + 60, width: alertView.frame.width - 32, height: 27.0))
            lblTitle.text = title
            lblTitle.textColor = .darkGray
            lblTitle.font = UIFont(name: "HelveticaNeue-Bold", size: 19.0)
            lblTitle.textAlignment = .center
            
            let subtitle = UILabel(frame: CGRect(x:16.0, y: imageAlert.frame.height + lblTitle.frame.height + 60, width: alertView.frame.width - 32, height: (CGFloat(description.count) / 30.0) * 27.0))
            subtitle.numberOfLines = 0
            subtitle.font = UIFont(name: "HelveticaNeue", size: 15.0)
            subtitle.text = description
            subtitle.textColor = .darkGray
            subtitle.textAlignment = .center
            
            
            let buttonRight = rightButton
            
            
            if(leftButton != nil) {
                let buttoneft = leftButton
                
                buttoneft!.frame = CGRect(x: 16, y: alertView.frame.height - 65, width: (alertView.frame.width / 2) - 24, height: 45)
                buttonRight.frame = CGRect(x: 40 + buttoneft!.frame.width, y: alertView.frame.height - 65, width: (alertView.frame.width / 2) - 32, height: 45)
                
                buttoneft!.backgroundColor = .systemBackground
                buttoneft!.borderWidth = 1.0
                buttoneft!.borderColor = .lightGray
                buttoneft!.setTitle(buttonTextLeft, for: .normal)
                buttoneft!.setTitleColor(.white, for: .normal)
                buttoneft!.layer.cornerRadius = 8.0
                alertView.addSubview(buttoneft!)
            }else {

                buttonRight.frame = CGRect(x: 16, y: alertView.frame.height - 65, width: alertView.frame.width - 32, height: 45)
            }
            
            buttonRight.backgroundColor = .red
            buttonRight.setTitle(buttonTextRight, for: .normal)
            buttonRight.setTitleColor(.white, for: .normal)
            buttonRight.layer.cornerRadius = 8.0
            
            imageAlert.image = nil
            
            switch type {
            case "success":
                imageAlert.image = #imageLiteral(resourceName: "img_ok")
            case "error":
                imageAlert.image = #imageLiteral(resourceName: "ic_error")
            case "info":
                imageAlert.image = #imageLiteral(resourceName: "ic_warning")
            default:
                imageAlert.image = #imageLiteral(resourceName: "img_ok")
            }
            
            
            alertView.addSubview(lblTitle)
            alertView.addSubview(buttonRight)
            alertView.addSubview(subtitle)
            alertView.addSubview(imageAlert)
        
        
        
        targetView.addSubview(backgroundView)
        targetView.addSubview(alertView)
        UIView.animate(withDuration: 0.25) {
            self.backgroundView.alpha = Constants.backgroundAlphaTo
        }
    }
    
}
protocol AddNoteDelegate {
    func xyz(note:String)
}
