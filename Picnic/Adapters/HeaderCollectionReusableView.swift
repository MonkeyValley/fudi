//
//  HeaderCollectionReusableView.swift
//  Picnic
//
//  Created by InnovacionVO on 23/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit

class HeaderCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var bgViewPromoTag: UIView!
    @IBOutlet weak var lblDescriptionCategory:UILabel!
    @IBOutlet weak var lblCounterCategory:UILabel!
    @IBOutlet weak var imgHeader:UIImageView!
}
