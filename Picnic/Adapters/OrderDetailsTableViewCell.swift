//
//  OrderDetailsTableViewCell.swift
//  Picnic
//
//  Created by InnovacionVO on 14/01/21.
//  Copyright © 2021 Monkey Valley. All rights reserved.
//

import UIKit

class OrderDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblPrice:UILabel!

}
