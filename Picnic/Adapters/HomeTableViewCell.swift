//
//  HomeTableViewCell.swift
//  Picnic
//
//  Created by InnovacionVO on 22/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imgBgHomeCell: UIImageView!
    @IBOutlet weak var tituloLugar:UILabel!
    @IBOutlet weak var sucursalLugar:UILabel!
    @IBOutlet weak var descripcionLugar:UILabel!
}
