//
//  SubSearchTableViewCell.swift
//  Picnic
//
//  Created by InnovacionVO on 22/03/21.
//  Copyright © 2021 Monkey Valley. All rights reserved.
//

import UIKit

class SubSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblSubPlace:UILabel!
}
