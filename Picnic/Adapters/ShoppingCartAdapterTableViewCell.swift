//
//  ShoppingCartAdapterTableViewCell.swift
//  Picnic
//
//  Created by InnovacionVO on 09/11/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit

class ShoppingCartAdapterTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitleITem:UILabel!
    @IBOutlet weak var lblDescriptionITem:UILabel!
    @IBOutlet weak var lblPRiceITem:UILabel!

}
