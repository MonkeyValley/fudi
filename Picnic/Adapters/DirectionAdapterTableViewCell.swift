//
//  DirectionAdapterTableViewCell.swift
//  Picnic
//
//  Created by InnovacionVO on 25/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit

class DirectionAdapterTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDirectionNameCell: UILabel!
    @IBOutlet weak var lblDirectionLineCell: UILabel!
    @IBOutlet weak var icDirectionCell: UIImageView!
}
