//
//  PlaceDetailsTableViewCell.swift
//  Picnic
//
//  Created by InnovacionVO on 22/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit

class PlaceDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitleCell: UILabel!
    @IBOutlet weak var lblDescriptionCell: UILabel!
    @IBOutlet weak var lblPriceCell: UILabel!
}
