//
//  HistoryAdapterTableViewCell.swift
//  Picnic
//
//  Created by InnovacionVO on 09/11/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit

class HistoryAdapterTableViewCell: UITableViewCell {

    @IBOutlet weak var bgViewSeparatorHistory: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblTotalToPay: UILabel!
}
