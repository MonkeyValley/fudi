//
//  BranchPlaceAdapterTableViewCell.swift
//  Picnic
//
//  Created by InnovacionVO on 24/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit

class BranchPlaceAdapterTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblTitleBranchCell: UILabel!
    @IBOutlet weak var lblPriceBranchCell: UILabel!
    @IBOutlet weak var lblNameBranchCell: UILabel!
    @IBOutlet weak var imgBranchCell: UIImageView!
    
}
