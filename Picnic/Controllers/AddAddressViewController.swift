//
//  AddAddressViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 25/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import CoreLocation


class AddAddressViewController: UIViewController {

    var address:[String:String]?
    
    @IBOutlet weak var txtNameAddress: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtNumberAddress: UITextField!
    @IBOutlet weak var txtNuberIntAddress: UITextField!
    @IBOutlet weak var txtReferencesAddress: UITextField!
    
    
    @IBOutlet weak var indicatorNameAddressView:UIView!
    @IBOutlet weak var indicatorAddressView:UIView!
    @IBOutlet weak var indicatorNumberIntView:UIView!
    @IBOutlet weak var indicatorNumberExtView:UIView!
    @IBOutlet weak var indicatorReferencesView:UIView!
    
    var centerCordinate:CLLocationCoordinate2D?
    var sharedPref = UserDefaults.standard
    var user:[String:Any]?
    var close:AddAddressAndCloseDelegate?
    var loadding:CustomLoadding = CustomLoadding()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if address != nil {
            txtAddress.text = address!["streetName"]
            txtNumberAddress.text = address!["streetNum"]
        }else{
            txtAddress.text = "--"
            txtNumberAddress.text = "--"
        }
        
        user = sharedPref.object(forKey: "_userData") as? [String: Any]
        let gesture = UIGestureRecognizer(target: self, action: #selector(self.onCloseKeyboard))
        view.addGestureRecognizer(gesture)
    }
    
    @IBAction func close(_ sender:Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addAddress(_ sender:Any){
        
        loadding.showLoadding(on: self)
        
        let nameAddress = txtNameAddress.text!
        let address = txtAddress.text!
        let numberext = txtNumberAddress.text!
        let numberint = txtNuberIntAddress.text!
        let reference = txtReferencesAddress.text!
        let colonia = self.address!["subLocality"] ?? ""
        let idUsuario = user!["id"] as? String ?? ""
        let lat = centerCordinate!.latitude
        let lng = centerCordinate!.longitude
        
        
        if((nameAddress != "" && nameAddress != "--") && (address != "" && address != "--") && (numberext != "" && numberext != "--") && (reference != "" && reference != "--")){
            
            requestHelper.guardarDireccion(usuarioId: idUsuario , _lat: lat, _lng: lng, colonia: colonia, nombreCalle: address, nombreDireccion: nameAddress, numeroExterior: numberext, numeroInterior: numberint, referencias: "", tipoDirecccion: 0) { (response) in
                
                if(!response!.bError){
                    let alert = UIAlertController(title: "¡Guardado!", message: "Dirección guardada correctamente.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: {_ in
                        self.close!.close()
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }else{
                    let alert = UIAlertController(title: "¡ERROR!", message: "La dirección no se pudo guardar correctamente. Intente de nuevo.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in
                        self.close!.close()
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                
                self.loadding.dismissAlert()

            }
        }else{
            self.loadding.dismissAlert()
            
            var msg:String = ""
            
            if(nameAddress == "" || nameAddress == "--")
            {
                msg = "\(msg) -Nombre de la dirección\n"
            }
            
            if(address == "" || address == "--")
            {
                msg = "\(msg) -Calle\n"
            }
            
            if(numberext == "" || numberext == "--")
            {
                msg = "\(msg) -Numero exterior\n"
            }
            
           
            if(reference == "" || reference == "--")
            {
                msg = "\(msg) -Referencias\n"
            }
            
            let alert = UIAlertController(title: "¡Espera!", message: "Faltan algunos datos: \n" + msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler:nil))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    @objc func onCloseKeyboard(){
        view.endEditing(true)
    }
}

extension AddAddressViewController:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case txtNameAddress:
            txtAddress.becomeFirstResponder()
            break
        case txtAddress:
            txtNumberAddress.becomeFirstResponder()
            break
        case txtNumberAddress:
            txtNuberIntAddress.becomeFirstResponder()
            break
        case txtNuberIntAddress:
            txtReferencesAddress.becomeFirstResponder()
            break
        case txtReferencesAddress:
            txtReferencesAddress.resignFirstResponder()
            break
        default:
            break
        }
        
        return true
    }
}
protocol AddAddressAndCloseDelegate {
    func close()
}
