//
//  HistoryViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 09/11/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import RealmSwift

class HistoryViewController: UIViewController {

    @IBOutlet weak var tvHistory:UITableView!
    
    var arrHistory:Results<OrdersModelRealm>!
    var realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getHistory()
        
    }
    
    func getHistory() {
        
        arrHistory = realm.objects(OrdersModelRealm.self).sorted(byKeyPath: "nFechaPedido", ascending: false)
        tvHistory.reloadData()
    }
}

extension HistoryViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell", for: indexPath) as! HistoryAdapterTableViewCell
        
        cell.bgViewSeparatorHistory.backgroundColor = .secondarySystemBackground
        
        cell.lblTitle.text = arrHistory[indexPath.row].pedido!.nombreRestaurante!
        cell.lblDescription.text = arrHistory[indexPath.row].pedido!.nombreSucursal
        cell.lblState.text = Utils.getStatusHistory(stauts:arrHistory[indexPath.row].status)
        
        cell.lblDay.text =  Utils.formatDay2Nubs(numb:arrHistory[indexPath.row].nDiaPedido)
        cell.lblMonth.text = Utils.getMonthFromNumMonth(month: arrHistory[indexPath.row].nMesPedido)
        cell.lblState.textColor = Utils.getColorFromNumStatus(stauts: arrHistory[indexPath.row].status)
        cell.lblTotalToPay.text = "$\(arrHistory[indexPath.row].pedido!.total)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "orderDetailsController") as! OrderDetailsViewController
        vc.idOrder = arrHistory![indexPath.row]._id!
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }
}
