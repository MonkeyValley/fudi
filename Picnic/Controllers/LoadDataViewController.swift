//
//  LoadDataViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 12/12/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import Lottie
import RealmSwift

class LoadDataViewController: UIViewController {

    @IBOutlet weak var animView: AnimationView!

    
    private var animatView: AnimationView?
    
    private let realm = try! Realm()
    let sharedPref = UserDefaults.standard
    var user:[String:AnyObject]?
    var userPayment:[String:AnyObject]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        animatView = .init(name: "splash")
        animatView!.frame = animView.bounds
        animatView!.frame.size.width = view.frame.width - 40
        animatView!.loopMode = .loop
        animatView!.contentMode = .scaleAspectFill
        animView.addSubview(animatView!)
        animatView!.play()
        user = sharedPref.value(forKey: "_userData") as? [String:AnyObject]
        userPayment = sharedPref.value(forKey: "_paymentData") as? [String:AnyObject]

        print(Realm.Configuration.defaultConfiguration.fileURL!)
        
        let result = realm.objects(UserAddress.self)
        print(user)
        print(result)
        
        if(result.count > 0){
            getPromos(address: result[0])
            getPedidos()
            getPlaces(address: result[0])
            getPayment()
            getDiscounts()
        }
        
    }
    
    private func getPlaces(address:UserAddress){
        requestHelper.getSucursales(regionEntrega: address.regionEntrega!) { (getSucursalesModel) in
            
            try! self.realm.write {
                self.realm.delete(self.realm.objects(sucursalModelReal.self))
                self.realm.delete(self.realm.objects(diaPromocionModelRealm.self))
            }
            
            try! self.realm.write{
        
                for i in getSucursalesModel!.data! {
                    
                    let model = sucursalModelReal()
                    model._id = i._id
                    model.bActivo = i.bActivo ?? false
                    model.cPassword = i.cPassword
                    model.cLogin = i.cLogin
                    model.dHoraCierre = i.dHoraCierre
                    model.dHoraApertura = i.dHoraApertura
                    model.cDireccion = i.cDireccion
                    model.nTiempoEntrega = i.nTiempoEntrega ?? 0
                    model.costoEnvio = i.costoEnvio ?? 0
                    model.cNombreSucursal = i.cNombreSucursal
                    model.nLatitud = i.nLatitud ?? 0
                    model.nLongitud = i.nLongitud ?? 0
                    model.nCostoEntrega = i.nCostoEntrega ?? 0
                    model.nTiempoEspera = i.nTiempoEspera ?? 0
                    model.nIdRestaurante = i.nIdRestaurante
                    model.TIME_ZONE = i.TIME_ZONE
                    model.nHoraApertura = i.nHoraApertura ?? 0
                    model.nHoraCierre = i.nHoraCierre ?? 0
                    model.nMinutoApertura = i.nMinutoApertura ?? 0
                    model.nMinutoCierre = i.nMinutoCierre ?? 0
                    model.regionEntrega = i.regionEntrega
                    model.bSoloRecoger = i.bSoloRecoger ?? false
                    model.cTelefono = i.cTelefono
                    model.tipoEntrega = i.tipoEntrega ?? 0
                    model.cNombreRestaurante = i.cNombreRestaurante
                    model.nIdCategoria = i.nIdCategoria ?? 0
                    model.cFoto = i.cFoto
                    model.nIdFormaPago = i.nIdFormaPago ?? 0
                    model.pedidoMinimo = i.pedidoMinimo ?? 0
                    model.nTipoDescuento = i.nTipoDescuento ?? 0
                    model.descuentoPicnic = i.descuentoPicnic ?? 0
                    
                    
                    
                    i.diasDescuentoPicnic?.forEach({ it in
                        let obj = diaPromocionModelRealm()
                        obj.bActivo = it.bActivo ?? false
                        obj.dia = it.dia ?? ""
                        obj.numeroDia = it.numeroDia ?? 0
                        obj.sucursalId = i._id ?? ""
                        self.realm.add(obj)
                    })
                    
                    
                    
                    self.realm.add(model)
                }
                self.printResults()
            }
            
            try! self.realm.write{
                self.realm.delete(self.realm.objects(CategoryModelRealm.self))
            }
            
            try! self.realm.write{
                if(getSucursalesModel!.data!.count > 0){
                    for i in getSucursalesModel!.data! {
                        
                        let results = self.realm.objects(CategoryModelRealm.self).filter("id == %@", i.nIdCategoria!)

                        
                        if(results.isEmpty){
                            
                            let object = CategoryModelRealm()
                            var titleCategory = "DEFAULT"
                            object.id = i.nIdCategoria!
                            object.count = 1
                            
                            switch i.nIdCategoria {
                            case 1:
                                    titleCategory = "SUSHI"
                                break
                            case 2:
                                titleCategory = "HAMBURGUESAS"
                                break
                            case 3:
                                titleCategory = "CHINA"
                                break
                            case 4:
                                titleCategory = "TACOS"
                                break
                            case 5:
                                titleCategory = "MARISCOS"
                                break
                            case 6:
                                titleCategory = "PIZZA"
                                break
                            case 7:
                                titleCategory = "MEXICANA"
                                break
                            case 8:
                                titleCategory = "CORTES"
                                break
                            case 9:
                                titleCategory = "ENSALADAS"
                                break
                            case 10:
                                titleCategory = "PASTELES"
                                break
                            default:
                                titleCategory = "DEFAULT"
                            }
                            
                            object.name = titleCategory
                            
                            self.realm.add(object)
                            
                            
                        }else{
                            results[0].count = results[0].count + 1
                        }
                    }
                    let resultCategories = self.realm.objects(CategoryModelRealm.self)
                    print(resultCategories)
                }
            }
            
            print("END -----------------------------")
            let storyboard = UIStoryboard(name: "Main", bundle: nil);
            let vc = storyboard.instantiateViewController(withIdentifier: "homeTabBarController") as! UITabBarController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)

            
        }
    }

    private func getPedidos(){
        let id = user!["id"] as? String ?? ""
        
        if(id != ""){
            requestHelper.getPedidosCliente(userId: id) { (getPedidosClienteModel) in
                print("getPedidosCliente --->")
                print(getPedidosClienteModel!.data![0].pedido?.lstPlatillos)
                print(getPedidosClienteModel)
                
                try! self.realm.write{
                    self.realm.delete(self.realm.objects(OrdersModelRealm.self))
                    self.realm.delete(self.realm.objects(PedidoModelRealm.self))
                    self.realm.delete(self.realm.objects(FormaPagoModelRealm.self))
                    self.realm.delete(self.realm.objects(PlatilloModelRealm.self))
                }
                
                try! self.realm.write{
                    if((getPedidosClienteModel?.data!.count)! > 0){
                        for (index,i) in getPedidosClienteModel!.data!.enumerated() {
                            
                            let model = OrdersModelRealm()
                            model._id = i._id
                            model.bPedidoGuardado = i.bPedidoGuardado
                            model.bCompraPicnic = i.bCompraPicnic
                            model.bPedidoInvitado = i.bCompraPicnic
                            model.fechaPedido = i.fechaPedido
                            model.grupo = i.grupo
                            model.nAnioPedido = i.nAnioPedido
                            model.nDiaPedido = i.nDiaPedido
                            model.nDiaSemanaPedido = i.nDiaSemanaPedido
                            model.nFechaPedido = i.nFechaPedido
                            model.nIdCategoria = i.nIdCategoria
                            model.nIdRestaurante = i.nIdRestaurante
                            model.nIdSucursal = i.nIdSucursal
                            model.nIdUsuario = i.nIdUsuario
                            model.nMesPedido = i.nMesPedido
                            model.nSemanaPedido = i.nSemanaPedido
                            
                                let pedidomodel = PedidoModelRealm()
                                pedidomodel.bCompraInvitado = i.pedido!.bCompraInvitado ?? false
                                pedidomodel.bRecoger = i.pedido!.bRecoger ?? false
                                pedidomodel.costoEnvio = i.pedido!.costoEnvio ?? 0
                                pedidomodel.fechaPedido = i.pedido!.fechaPedido ?? ""
                                    
                                    let formadepago = FormaPagoModelRealm()
                                    formadepago.idTarjeta = i.pedido!.formaDePago?.idTarjeta
                                    formadepago.device_session_id = i.pedido!.formaDePago?.idTarjeta
                                    formadepago.nTipoPago = i.pedido!.formaDePago!.nTipoPago ?? 0
                                    formadepago.pagaCon = i.pedido!.formaDePago!.pagaCon ?? 0
                                    pedidomodel.formaDePago = formadepago
                                
                                pedidomodel.index = i.pedido!.index ?? 0
                                pedidomodel.nIdCategoria = i.pedido?.nIdCategoria ?? 0
                                pedidomodel.nIdRestaurante = i.pedido!.nIdRestaurante ?? ""
                                pedidomodel.nIdSucursal = i.pedido!.nIdSucursal ?? ""
                                pedidomodel.nIdUsuario = i.pedido!.nIdUsuario ?? ""
                                pedidomodel.nombreRestaurante = i.pedido!.nombreRestaurante ?? ""
                                pedidomodel.nombreSucursal = i.pedido!.nombreSucursal ?? ""
                                pedidomodel.playerId = i.pedido!.playerId ?? ""
                                pedidomodel.regionEtrega = i.pedido!.regionEtrega ?? ""
                                pedidomodel.statusPedido = i.pedido!.statusPedido ?? 0
                                pedidomodel.subTotal = i.pedido!.subTotal ?? 0.0
                                pedidomodel.total = i.pedido!.total ?? 0.0
                        
                        
                                model.pedido = pedidomodel
                            model.regionEntrega = i.regionEntrega
                            model.status = i.status
                            model.tipoCompra = i.tipoCompra
                            
                            self.realm.add(model)
                            
                            for item in getPedidosClienteModel!.data![0].pedido!.lstPlatillos! {
                                let platilloModel = PlatilloModelRealm()
                                platilloModel._id = item._id!
                                platilloModel.bFoto = item.bFoto!
                                platilloModel.bIngredienteElegir = item.bIngredienteElegir!
                                platilloModel.bPlatilloHome = item.bPlatilloHome!
                                platilloModel.cDescripcion = item.cDescripcion!
                                platilloModel.cFoto = item.cFoto!
                                platilloModel.cNombrePlatillo = item.cNombrePlatillo!
                                platilloModel.comentarios = item.comentarios!
                                platilloModel.nIdCategoria = item.nIdCategoria!
                                platilloModel.nIdRestaurante = item.nIdRestaurante!
                                platilloModel.nIngredientes = item.nIngredientes!
                                platilloModel.nPrecio = item.nPrecio!
                                platilloModel.orderId = i._id
                                platilloModel.quantity = item.quantity!
                                
                                self.realm.add(platilloModel)
                            }
                            
                        }
                        let resultOrders = self.realm.objects(OrdersModelRealm.self)
                        print(resultOrders)
                    }
                }
                
            }
        }
    }
    
    private func getPromos(address:UserAddress){
        requestHelper.getPromociones(regionEntrega: address.regionEntrega!) { (getPromocionesModel) in
            
            try! self.realm.write {
                self.realm.delete(self.realm.objects(PromosModelRealm.self))
            }
        
            try! self.realm.write {
                for i in getPromocionesModel!.data! {
                    
                    let obj = PromosModelRealm()
                    obj._id = i._id ?? ""
                    obj.bAccionBanner = i.bAccionBanner ?? false
                    obj.bRestriccionDia = i.bRestriccionDia ?? false
                    obj.bRestriccionHora = i.bRestriccionHora ?? false
                    obj.cDescripcion = i.cDescripcion ?? ""
                    obj.cFoto = i.cFoto ?? ""
                    obj.cNombrePromocion = i.cNombrePromocion ?? ""
                    obj.horaFin = i.horaFin ?? 0
                    obj.horaInicio = i.horaInicio ?? 0
                    obj.minutoFin = i.minutoFin ?? 0
                    obj.minutoInicio = i.minutoInicio ?? 0
                    obj.nIdCategoria = i.nIdCategoria ?? 0
                    obj.nIdRestaurante = i.nIdRestaurante ?? ""
                    obj.nPrecio = i.nPrecio ?? 0
                    obj.nombreRestaurante = i.nombreRestaurante ?? ""
                    
                    let arrs = List<PromotionDayModelRealm>()
                    for j in i.lstDiasPromocion! {
                        let model = PromotionDayModelRealm()
                        model.bActivo = j.bActivo!
                        model.dia = j.dia
                        model.numeroDia = j.numeroDia!
                        arrs.append(model)
                    }
                    obj.dynamicList(arrs)

                    self.realm.add(obj)
                }
                let resultPromotions = self.realm.objects(PromosModelRealm.self)
                print(resultPromotions)
            }
        }
    }
    
    func getPayment() {
        requestHelper.getCardsCustomer(openPayUserId: userPayment!["id"] as? String ?? "") { (getCardsCustomerModel) in
            
            print(getCardsCustomerModel)
            try! self.realm.write{
                self.realm.delete(self.realm.objects(PaymentsModelRealm.self))
            }
            
            
            try! self.realm.write{
                
                for i in getCardsCustomerModel!.data! {
                    
                    let obj = PaymentsModelRealm()
                    obj.id = i.id ?? ""
                    obj.type = i.type ?? ""
                    obj.brand = i.brand ?? ""
                    obj.address = i.address ?? ""
                    obj.card_number = i.card_number ?? ""
                    obj.holder_name = i.holder_name ?? ""
                    obj.expiration_year = i.expiration_year ?? ""
                    obj.expiration_month = i.expiration_year ?? ""
                    obj.allows_charges = i.allows_charges ?? false
                    obj.allows_payouts = i.allows_payouts ?? false
                    obj.creation_date = i.creation_date ?? ""
                    obj.bank_name = i.bank_name ?? ""
                    obj.bank_code = i.bank_code ?? ""
                    obj.points_type = i.points_type ?? ""
                    obj.customer_id = i.customer_id ?? ""
                    obj.points_card = i.points_card ?? false
 
                    self.realm.add(obj)
                }
                let resultPayements = self.realm.objects(PaymentsModelRealm.self)
                print(resultPayements)
                
            }
        }
    }
    
    func getDiscounts(){
        let id = user!["id"] as? String ?? ""

        requestHelper.getCupones(userId: id) { respuesta in
        
            if(!respuesta!.bError!){
                print(respuesta)
                
                try! self.realm.write{
                    self.realm.delete(self.realm.objects(DiscountsModelRealm.self))
                }
                
                try! self.realm.write{
                    
                    for i in respuesta!.data! {
                        
                        let obj = DiscountsModelRealm()
                        obj._id = i._id
                        obj.nIdUsuario = i.nIdUsuario
                        obj.nIdCupon = i.nIdCupon
                        obj.bCanjeado = i.bCanjeado
                        obj.cCodigoCupon = i.cCodigoCupon
                        obj.cDescripcion = i.cDescripcion
                        obj.cNombreRestaurante = i.cNombreRestaurante
                        obj.nIdRestaurante = i.nIdRestaurante
                        obj.nTipoCupon = i.nTipoCupon
                        obj.nValorCupon = i.nValorCupon
     
                        self.realm.add(obj)
                    }
                    let resultPayements = self.realm.objects(DiscountsModelRealm.self)
                    print(resultPayements)
                    
                }
            }
            
        }
    }
    
    func printResults() {
        let resultSucursals = realm.objects(sucursalModelReal.self)
        print(resultSucursals)
    }
}
