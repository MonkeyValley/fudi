//
//  ProductDetailsViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 23/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import Kingfisher
import HorizontalProgressBar
import RealmSwift


class ProductDetailsViewController: UIViewController {

    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var lblNumberProduct:UILabel!
    
    @IBOutlet weak var btnType1:UIButton!
    @IBOutlet weak var btnType2:UIButton!
    @IBOutlet weak var btnType3:UIButton!
    @IBOutlet weak var btnType4:UIButton!
    @IBOutlet weak var horizontalLoading:UIView!
    @IBOutlet weak var loadingAddToCart:UIActivityIndicatorView!
    @IBOutlet weak var btnAddToCart:UIButton!
    
    @IBOutlet weak var imgPlate: UIImageView!
    
    var numberProduct = 1
    var btnSelected:UIButton?
    var typeSelected:Int = -1
    var addtocart:AddtoCartDelegate?
    var customAlert = CustoAlert()
    var data:platilloModel?
    var dataPromo:PromosModelRealm?
    var userdefaults = UserDefaults.standard
    var category = 0
    var note = ""
    var nIdSucursal = ""
    let realm = try! Realm()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(data != nil){
            initData(data: self.data!)
        }else if(dataPromo != nil){
            btnAddToCart.backgroundColor = UIColor(red: 254/255, green: 70/255, blue: 0/255, alpha: 1.0)
            btnAddToCart.isEnabled = true
            initDataPromo(data: dataPromo!)
        }else{
            self.dismiss(animated: true, completion: nil)
            self.addtocart!.error()
        }
        loadingAddToCart.stopAnimating()
        
    }
    
    func initData(data:platilloModel) {
        
           lblTitle.text = data.cNombrePlatillo!
           lblDescription.text = data.cDescripcion!
           lblPrice.text = "$\(data.lstTamanios![0].nPrecio!)"
           imgPlate.kf.setImage(with: URL(string: "\(Constantes.IMAGE_BASE)\(data.cFoto!)"), completionHandler: {result in
               switch result {
               case .success( _):
                   break
               case .failure( _):
                   self.imgPlate.image = UIImage(named: "img_noimage_transparent")
                   break
               }
           })
           var index = 0
           for i in data.lstTamanios! {
               
               switch index {
               case 0:
                   
                   btnType1.isHidden = false
                   btnType1.setTitle( i.cNombreTamanio , for: .normal)
                   break
               case 1:
                   btnType2.isHidden = false
                   btnType2.setTitle(i.cNombreTamanio, for: .normal)
                   break
               case 2:
                   btnType3.isHidden = false
                   btnType3.setTitle(i.cNombreTamanio, for: .normal)
                   break
               case 3:
                   btnType4.isHidden = false
                   btnType4.setTitle(i.cNombreTamanio, for: .normal)
                   break
               default:
                   btnType1.isHidden = true
                   btnType2.isHidden = true
                   btnType3.isHidden = true
                   btnType4.isHidden = true
                   
                   break
               }
               index += 1
           }
    }
    
    func initDataPromo(data:PromosModelRealm) {
        
        lblTitle.text = data.cNombrePromocion
           lblDescription.text = data.cDescripcion!
        lblPrice.text = "$\(data.nPrecio)"
           imgPlate.kf.setImage(with: URL(string: "\(Constantes.IMAGE_BASE)\(data.cFoto!)"), completionHandler: {result in
               switch result {
               case .success( _):
                   break
               case .failure( _):
                   self.imgPlate.image = UIImage(named: "img_noimage_transparent")
                   break
               }
           })
    }
    
    func addToCart() {
        
        self.dismiss(animated: true, completion: nil)
        let allItems = realm.objects(CartModelRealm.self)
                 
        try! realm.write {
            
            let getitem = realm.objects(CartModelRealm.self).filter("nIdRestaurante == '\(String(data!.nIdRestaurante!))'")
            
            if(getitem.count == 0){
                self.realm.delete(self.realm.objects(CartModelRealm.self))
            }
            
            let getOneItem = realm.objects(CartModelRealm.self).filter("idProduct == '\(String(data!._id!))' AND sizeProduct == '\(String(self.data!.lstTamanios![self.typeSelected - 1].cNombreTamanio!))'")
            
            
            if let itemtoEdit = getOneItem.first{
                itemtoEdit.quantity += numberProduct
            }else{
                print(category)
                let item = CartModelRealm()
                item._id = allItems.count
                item.idProduct = data!._id
                item.name = data!.cNombrePlatillo
                item.note = self.note
                item.quantity = numberProduct
                item.nIdRestaurante = data!.nIdRestaurante!
                item.nIdSucursal = nIdSucursal
                item.sizeProduct = self.data!.lstTamanios![self.typeSelected - 1].cNombreTamanio
                item.price = Double(self.data!.lstTamanios![self.typeSelected - 1].nPrecio!)
                item.category = self.category
                realm.add(item)
            }
            
        }
        loadingAddToCart.stopAnimating()
        addtocart!.add()
    }
    
    func addToCartPromo() {
        self.dismiss(animated: true, completion: nil)
        let allItems = realm.objects(CartModelRealm.self)
                 
            try! realm.write {
            let getOneItem = realm.objects(CartModelRealm.self).filter("idProduct == '\(String(data!._id!))' AND sizeProduct == '\(String(self.data!.lstTamanios![self.typeSelected - 1].cNombreTamanio!))'")
            if let itemtoEdit = getOneItem.first{
                itemtoEdit.quantity += numberProduct
            }else{
                
                print(category)
                let item = CartModelRealm()
                item._id = allItems.count
                item.idProduct = dataPromo!._id
                item.name = dataPromo!.cNombrePromocion
                item.note = self.note
                item.quantity = numberProduct
                item.sizeProduct = ""
                item.price = Double(self.data!.nPrecio!)
                item.category = self.category
                realm.add(item)
            }
        }
        loadingAddToCart.stopAnimating()
        addtocart!.add()
    }
    
    @IBAction func actionSelectSize(_ sender:UIButton){
        
        if(btnSelected != nil){
            btnSelected!.backgroundColor = .opaqueSeparator
            sender.backgroundColor = UIColor(red: 254/255, green: 70/255, blue: 0/255, alpha: 1.0)
            
            btnSelected = sender
            
        }else{
            btnSelected = sender
            btnSelected!.backgroundColor = .opaqueSeparator
            sender.backgroundColor = UIColor(red: 254/255, green: 70/255, blue: 0/255, alpha: 1.0)
        }
        typeSelected = sender.tag
        
        btnAddToCart.backgroundColor = UIColor(red: 254/255, green: 70/255, blue: 0/255, alpha: 1.0)
        btnAddToCart.isEnabled = true
    }
    
    @IBAction func actionAddNotes(_ sender:UIButton){
        customAlert.showBottomSheet(whit: "", messagge: "", on: self, delegate:self)
    }
    
    @IBAction func actionAdd(_ sender:UIButton){
        
        if(sender.tag == 1){
            numberProduct += 1
        }else if(numberProduct > 1){
            numberProduct -= 1
        }
        lblNumberProduct.text = String(numberProduct)
        
    }

    @IBAction func actionAddToCart(_ sender: UIButton) {
        loadingAddToCart.startAnimating()
        if(data != nil){
            self.addToCart()
        }else{
            self.addToCartPromo()
        }
    }
    
    
}
protocol AddtoCartDelegate {
    func add()
    func error()
}
extension ProductDetailsViewController:AddNoteDelegate{
    func xyz(note:String) {
        print(note)
        self.note = note
    }
}
