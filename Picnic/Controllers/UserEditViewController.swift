//
//  UserEditViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 26/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import RealmSwift

class UserEditViewController: UIViewController {

    @IBOutlet weak var viewResetPassword: UIView!
    
    @IBOutlet weak var txtNameUser:UITextField!
    @IBOutlet weak var txtPhoneUser:UITextField!
    @IBOutlet weak var txtEmailUser:UITextField!
    
    @IBOutlet weak var txtNewPassword:UITextField!
    @IBOutlet weak var txtNewRePassword:UITextField!
    
    let sharedPref = UserDefaults.standard
    private let realm = try! Realm()
    var user:[String:AnyObject]?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        getUserinfo()
        
    }
    
    func getUserinfo() {
        user = sharedPref.value(forKey: "_userData") as? [String:AnyObject]
        
        
        txtNameUser.text = user!["userName"] as? String ?? ""
        txtPhoneUser.text = user!["phone"] as? String ?? ""
        txtEmailUser.text = user!["mail"] as? String ?? ""
        
        
    }
    
    @IBAction func actionClose(_ sender:UIButton){
        self.dismiss(animated: true)
    }

    @IBAction func actionResetPasword(_ sender: UIButton) {
        if sender.tag == 0{
            sender.setImage(#imageLiteral(resourceName: "ic_check"), for: .normal)
            viewResetPassword.isHidden = false
            sender.tag = 1
            
        }else{
            sender.setImage(#imageLiteral(resourceName: "ic_unchecked"), for: .normal)
            viewResetPassword.isHidden = true
            sender.tag = 0
        }
    }
}
