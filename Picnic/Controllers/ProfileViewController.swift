//
//  ProfileViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 22/01/21.
//  Copyright © 2021 Monkey Valley. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblPhone:UILabel!
    @IBOutlet weak var imgUser:UIImageView!
    
    var sharedPref = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let user = sharedPref.object(forKey: "_userData") as? [String: Any]

        let name = user?["userName"] as? String ?? ""
        let phone = user?["phone"] as? String ?? ""
        
        lblName.text = name
        lblPhone.text = phone
    }
    
    
}
