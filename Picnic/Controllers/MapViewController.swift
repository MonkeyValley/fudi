//
//  MapViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 10/11/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController {

    @IBOutlet weak var mkLocation:MKMapView!
    @IBOutlet weak var lblAddresName: UILabel!
    var locationManager = CLLocationManager()
    var address = [String:String]()
    var centerCoordinate:CLLocationCoordinate2D?
    override func viewDidLoad() {
        super.viewDidLoad()
        checkLocationService()
        // Do any additional setup after loading the view.
    }
    
    func setupLocationManager(){
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
    }
    func checkLocationService(){
        if(CLLocationManager.locationServicesEnabled()){
            setupLocationManager()
            checkLocationAuthorization()
            locationManager.startUpdatingLocation()
        }else{
            
        }
    }
    func centerViewUSerLocation(c:CLLocationCoordinate2D){
        let region = MKCoordinateRegion.init(center: c, latitudinalMeters: 1000, longitudinalMeters: 1000)
        mkLocation.setRegion(region, animated: true)
    }
    func checkLocationAuthorization(){
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            //JALADOS
            mkLocation.showsUserLocation = true
            if let location = locationManager.location?.coordinate{
                centerViewUSerLocation(c:location)
                locationManager.startUpdatingLocation()
            }
            
            break
        case .denied:
            //Muestra una alerta con los pasos para encender la localizacion
        break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        break
        case .restricted:
            //uetsrale una alerta explicado que pasa
        break
        case .authorizedAlways:
        break
        default:
            //ERROR
            break
        }
    }

    @IBAction func actionCloseActivity(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionContinue(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "addaddressController") as! AddAddressViewController
        vc.address = self.address
        vc.centerCordinate = self.centerCoordinate
        vc.close = self
        self.present(vc, animated: true, completion: nil);
    }
}
extension MapViewController:CLLocationManagerDelegate, MKMapViewDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last?.coordinate{
            centerViewUSerLocation(c: location)
            locationManager.stopUpdatingLocation()
        }
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        centerCoordinate = mapView.centerCoordinate
        
        let center = CLLocation(latitude: self.centerCoordinate!.latitude, longitude: self.centerCoordinate!.longitude)
        let geoCoder = CLGeocoder()

        geoCoder.reverseGeocodeLocation(center){[weak self](placemark, error) in
            guard self != nil else {return}
            
            if let _ = error{
                return
            }
            
            guard let placemark = placemark?.first else{ return }
            
            let streetName = placemark.thoroughfare ?? "--"
            let streetNum = placemark.subThoroughfare ?? "--"
            
            
            
            let country = placemark.country ?? "--"
            let zipcode = placemark.postalCode ?? "--"
            
            let name = placemark.name ?? "--"
            let administrativeArea = placemark.administrativeArea ?? "--"
            let locality = placemark.locality ?? "--"
            let subAdministrativeArea = placemark.subAdministrativeArea ?? "--"
            let subLocality = placemark.subLocality ?? "--"
            
            print("streetName, \(streetName)")
            print("streetNum, \(streetNum)")
            print("country, \(country)")
            print("zipcode, \(zipcode)")
            print("name, \(name)")
            print("administrativeArea, \(administrativeArea)")
            print("locality, \(locality)")
            print("subAdministrativeArea, \(subAdministrativeArea)")
            print("areasOfInterest, \(placemark.areasOfInterest ?? [])")
            print("subLocality, \(placemark.subLocality ?? "" )")
            print("subThoroughfare, \(placemark.subThoroughfare ?? "" )")
            
            

            DispatchQueue.main.async {
                self!.lblAddresName.text = "\(streetName) \(streetNum), \(subLocality), \(locality) \(administrativeArea), \(zipcode), \(country)"
                
                self!.address.updateValue(streetName, forKey: "streetName")
                self!.address.updateValue(streetNum, forKey: "streetNum")
                self!.address.updateValue(subLocality, forKey: "subLocality")
                self!.address.updateValue(locality, forKey: "locality")
                self!.address.updateValue(administrativeArea, forKey: "administrativeArea")
                self!.address.updateValue(subAdministrativeArea, forKey: "subAdministrativeArea")
                self!.address.updateValue(zipcode, forKey: "zipcode")
                self!.address.updateValue(country, forKey: "country")
                
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
}
extension MapViewController: AddAddressAndCloseDelegate{
    func close() {
        self.dismiss(animated: true, completion: nil)
    }
}
