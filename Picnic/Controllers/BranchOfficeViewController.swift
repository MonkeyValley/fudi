//
//  BranchOfficeViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 24/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import RealmSwift

class BranchOfficeViewController: UIViewController {

    @IBOutlet weak var tvSucursals:UITableView!
    
    var arrPromos:Results<PromosModelRealm>!
    var realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        gteSucursal()
    }
    
    func gteSucursal() {
        
        arrPromos = realm.objects(PromosModelRealm.self)
        tvSucursals.reloadData()
    }

}
extension BranchOfficeViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPromos.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "branchofficecell", for: indexPath) as! BranchPlaceAdapterTableViewCell
        
        
        cell.lblTitleBranchCell.text = arrPromos[indexPath.row].cNombrePromocion
        cell.lblNameBranchCell.text = arrPromos[indexPath.row].nombreRestaurante
        cell.lblPriceBranchCell.text = "$\(arrPromos[indexPath.row].nPrecio)"

        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220.0
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*let item = arrPromos![indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "placedetailsController") as! PlaceDatailsViewController
        vc.idPlace = item.nIdRestaurante!
        vc.categoryId = item.nIdCategoria
        self.present(vc, animated: true, completion: nil)*/
        let item = arrPromos![indexPath.row]
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "productdetailcontroller") as! ProductDetailsViewController
        vc.addtocart = self
        vc.dataPromo = item
        self.present(vc, animated: true, completion: nil);
    }
}


extension BranchOfficeViewController: AddtoCartDelegate{
    func error() {
        Toast.showAlert(with: "Platillo no encontrado.", on: self, time: "long")
    }
    func add() {
        self.dismiss(animated: true, completion: nil)
    }
}
