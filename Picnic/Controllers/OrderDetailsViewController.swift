//
//  OrderDetailsViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 07/01/21.
//  Copyright © 2021 Monkey Valley. All rights reserved.
//

import UIKit
import RealmSwift
import CoreLocation
import MapKit


class OrderDetailsViewController: UIViewController {

    @IBOutlet weak var layerViewHowTogo:UIView!
    @IBOutlet weak var lblStatusDelivery:UILabel!
    
    @IBOutlet weak var btnHowTodrive:UIButton!
    @IBOutlet weak var btnContinue:UIButton!
    
    @IBOutlet weak var layerHowTodrive:UIView!
    @IBOutlet weak var layerContinue:UIView!
    
    var realm = try! Realm()
    var idOrder:String=""
    var arrPlatillos:Results<PlatilloModelRealm>?
    var order:OrdersModelRealm?
    var sucursal:sucursalModelReal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrPlatillos = realm.objects(PlatilloModelRealm.self).filter("orderId == '\(String(idOrder))'")
        order = realm.objects(OrdersModelRealm.self).filter("_id == '\(String(idOrder))'").first ?? OrdersModelRealm()
        
        if(order != nil){
            lblStatusDelivery.text = Utils.getStatusHistory(stauts: order!.status)
            sucursal = realm.objects(sucursalModelReal.self).filter("_id == '\(String(order!.nIdSucursal!))'").first ?? sucursalModelReal()
            
            
            
            if(order!.pedido!.bRecoger){
                layerContinue.isHidden = true
                layerHowTodrive.isHidden = false
            }else{
                layerContinue.isHidden = false
                layerHowTodrive.isHidden = true
            }
        }
    }
    
    func actionAlertSheet(){
        let alert = UIAlertController(title: "Ayuda pedido", message: "Selecciona una opción", preferredStyle: .actionSheet)
        let phoneNumber = "3111492115"
        
        alert.addAction(UIAlertAction(title: "Marcar a sucursal", style: .default , handler:{ (UIAlertAction)in
            if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {

                  let application:UIApplication = UIApplication.shared
                  if (application.canOpenURL(phoneCallURL)) {
                      if #available(iOS 10.0, *) {
                          application.open(phoneCallURL, options: [:], completionHandler: nil)
                      } else {
                          // Fallback on earlier versions
                           application.openURL(phoneCallURL as URL)

                      }
                  }
              }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler:{ (UIAlertAction)in
            alert.dismiss(animated: true, completion: nil)
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    func openMapForPlace(latitude:CLLocationDegrees, longitude:CLLocationDegrees) {
        let coordinate = CLLocationCoordinate2DMake(latitude,longitude)
        let regionDistance:CLLocationDistance = 5000
        let regionSpan = MKCoordinateRegion(center: coordinate, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
                    MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                    MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "Target location"
        mapItem.openInMaps(launchOptions: options)
           

        //mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
    }
    
    @IBAction func actionBack(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionWhereToDrive(_ sender:UIButton){
        if sucursal != nil {
            let lat:CLLocationDegrees = sucursal!.nLatitud
            let lng:CLLocationDegrees = sucursal!.nLongitud
            
            openMapForPlace(latitude: lat, longitude: lng)
        }
    }
    @IBAction func actionHelp(_ sender:UIButton){
        actionAlertSheet()
        
    }
}
extension OrderDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderDetailsCell", for: indexPath) as! OrderDetailsTableViewCell
        cell.lblTitle.text = arrPlatillos![indexPath.row].cNombrePlatillo
        cell.lblPrice.text = "$\(arrPlatillos![indexPath.row].nPrecio)"
        if(order != nil){
            cell.lblDescription.text = Utils.getEoticonsfromCategory(idCategory: order?.nIdCategoria ?? 0)
        }else{
            cell.lblDescription.text = arrPlatillos![indexPath.row].cDescripcion
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPlatillos?.count ?? 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 79.0
    }
}
