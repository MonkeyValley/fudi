//
//  SubSearchViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 22/03/21.
//  Copyright © 2021 Monkey Valley. All rights reserved.
//

import UIKit
import RealmSwift

class SubSearchViewController: UIViewController {

    @IBOutlet weak var txtSearch:UITextField!
    @IBOutlet weak var tvLugares:UITableView!
    
    var lugaresArray:Results<sucursalModelReal>?
    var emptyArray: Array<sucursalModelReal> = Array()
    private let realm = try! Realm()
    var category = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtSearch.becomeFirstResponder()
        obtenerLugaresPorCategoria()
        
    }

    func obtenerLugaresPorCategoria(){
        lugaresArray = self.realm.objects(sucursalModelReal.self)
        clearList()
    }
    func clearList(){
        for item in lugaresArray! {
            emptyArray.append(item)
        }
        tvLugares.reloadData()
    }
}
extension SubSearchViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        emptyArray.removeAll()
        
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let character = textFieldText.replacingCharacters(in: range, with: string)

        if(character != ""){
            let matches = self.lugaresArray!.filter { word in
                return word.cNombreRestaurante!.lowercased().contains(character.lowercased())
            }
            emptyArray = Array(matches)
            tvLugares.reloadData()
        }else{
            clearList()
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.dismiss(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}

extension SubSearchViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emptyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subsearchcell", for: indexPath) as! SubSearchTableViewCell
        
        cell.lblDescription.text = Utils.getEoticonsfromCategory(idCategory: emptyArray[indexPath.row].nIdCategoria)
        cell.lblSubPlace.text = emptyArray[indexPath.row].cNombreSucursal
        cell.lblTitle.text = emptyArray[indexPath.row].cNombreRestaurante
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 77.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = self.emptyArray[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "placedetailsController") as! PlaceDatailsViewController
        vc.idPlace = item.nIdRestaurante!
        vc.namePlace = item.cNombreRestaurante!
        vc.subPlace = item.cNombreSucursal!
        vc.categoryId = item.nIdCategoria
        vc.closeWindows = self
        vc.idSubPlace = item._id!
        self.present(vc, animated: true, completion:nil)
        
    }
}
extension SubSearchViewController:HandlerDelegate{
    func closeWindow() {
        self.dismiss(animated: true)
    }
}
protocol HandlerDelegate {
    func closeWindow()
}

