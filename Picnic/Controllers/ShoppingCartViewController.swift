//
//  ShoppingCartViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 09/11/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import RealmSwift

class ShoppingCartViewController: UIViewController {

    @IBOutlet weak var tvCheckout: UITableView!
    @IBOutlet weak var lblAddresTitle:UILabel!
    @IBOutlet weak var lblAddresDescription:UILabel!
    @IBOutlet weak var lblMethoPayTitle:UILabel!
    @IBOutlet weak var lblMethoPayDesc:UILabel!
    @IBOutlet weak var lblSubTotalToPay:UILabel!
    @IBOutlet weak var lblDeliveryCost:UILabel!
    @IBOutlet weak var lblDiscount:UILabel!
    @IBOutlet weak var lblTotal:UILabel!
    @IBOutlet weak var icPromotion:UIImageView!
    
    @IBOutlet weak var skvDeliveryCost:UIStackView!
    @IBOutlet weak var skvDiscount:UIStackView!
    
    @IBOutlet weak var heightDeliveryCost:NSLayoutConstraint!
    @IBOutlet weak var heightDiscount:NSLayoutConstraint!

    @IBOutlet weak var btnAddCuppon:UIButton!
    @IBOutlet weak var btnOrder:UIButton!
    
    var addresSelected:[String:Any]!
    var realm = try! Realm()
    let sharedPref = UserDefaults.standard
    var arrShoppingCart:Results<CartModelRealm>!
    var arrPromotionsDays:Results<diaPromocionModelRealm>!
    var arrDiscount:DiscountApplyModelRealm?
    var sucursal:sucursalModelReal!
    var addressObject:UserAddress!
    var subtotal = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrDiscount = realm.objects(DiscountApplyModelRealm.self).first
        arrShoppingCart = realm.objects(CartModelRealm.self)
        sucursal = realm.objects(sucursalModelReal.self).filter("_id == '\(String(arrShoppingCart[0].nIdSucursal))'").last
        if(arrShoppingCart != nil && arrShoppingCart.count > 0){
            arrPromotionsDays = realm.objects(diaPromocionModelRealm.self).filter("sucursalId == '\(String(arrShoppingCart[0].nIdSucursal))'")
        }
        print(arrShoppingCart)
        print(sucursal)
        getAddresUser()
        calculateTotals()

    }
    
    func calculateTotals () {
        
        var totaldiscount = 0.0
        var totaldelivery = 0.0
        subtotal = 0.0
        
        arrShoppingCart.forEach { item in
            let subtotalMult = Double(item.quantity) * item.price
            subtotal += subtotalMult
        }
        
        var istDiscount = false
        arrPromotionsDays.forEach { (it) in
            if(it.numeroDia == getDayOfWeek()){
                istDiscount = it.bActivo
            }
        }
        
        if(sucursal.nTipoDescuento == 2){
            if istDiscount {
                print(sucursal.descuentoPicnic)
                
                let discPrecent = Double(sucursal.descuentoPicnic)/100
                totaldiscount = subtotal * discPrecent
                
                lblDiscount.text = "$" + String(totaldiscount)
                heightDiscount.constant = 26
            }else{
                heightDiscount.constant = 0
            }
        }else{
            heightDiscount.constant = 0
        }
        
        
        if(arrDiscount != nil){
            
            if sucursal.nIdRestaurante == arrDiscount?.nIdRestaurante{
                icPromotion.isHidden = true
                heightDiscount.constant = 26
                switch arrDiscount!.nTipoCupon {
                case 2:
                    let discPrecent = Double(arrDiscount!.nValorCupon)
                    totaldiscount = totaldiscount + discPrecent
                    lblDiscount.text = "-$" + String(totaldiscount)
                    break
                case 3:
                    lblDiscount.text = "Gratis"
                    lblDiscount.font = UIFont.boldSystemFont(ofSize: 15.0)
                    break
                case 4:
                    break
                case 5:
                    break
                case 1:
                    icPromotion.isHidden = false
                    icPromotion.image = #imageLiteral(resourceName: "ic_promo_red")
                    let discPrecent = Double(arrDiscount!.nValorCupon) / 100.0
                    totaldiscount = totaldiscount + (subtotal * discPrecent)
                    lblDiscount.text = "-$" + String(totaldiscount)
                    break
                default:
                    break
                }
            
            }
        }
        
        
        if(addresSelected != nil){
            let addresName = addresSelected!["nombreDireccion"] as? String ?? ""
            if(addresName != "" && addresName != "Recoger"){
                totaldelivery = Double(sucursal.nCostoEntrega)
            }
        }
        
        
        
        lblSubTotalToPay.text = "$" + String(subtotal)
        lblTotal.text = "$" + String(subtotal + totaldelivery - totaldiscount)
        
    }
    func getAddresUser() {
        addresSelected = sharedPref.object(forKey: "_addressSelected") as? [String: Any]
        if(addresSelected != nil){
            let addresName = addresSelected!["nombreDireccion"] as? String ?? ""
            let numExt = addresSelected!["numeroExterior"] as? String ?? ""
            let calle = addresSelected!["nombreCalle"] as? String ?? ""
            let colonia = addresSelected!["colonia"] as? String ?? ""
            
           
            
            if(addresName == "" || addresName == "Recoger"){
                lblAddresTitle.text = "Recoger"
                lblAddresDescription.text = "Pide y recoge."
                heightDeliveryCost.constant = 0
                
            }else{
                let costoEntrega = Double(sucursal.nCostoEntrega)
                lblDeliveryCost.text = "$" + String(costoEntrega)
                heightDeliveryCost.constant = 26
                lblAddresTitle.text = addresName
                lblAddresDescription.text = "\(calle) \(numExt), \(colonia)"
            }
            
        }else{
            lblAddresTitle.text = "Sin domicilio seleccionado"
            lblAddresDescription.text = "Selecciona un domicilio para continuar."
            heightDeliveryCost.constant = 0
            btnOrder.isHidden = true
        }
    }
    
    func getDayOfWeek() -> Int? {
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: Date())
        return weekDay
    }
    
    @IBAction func actionAddCupon(_ sender:UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "discountsController") as! DiscountsViewController
        vc.nIdRestaurante = arrShoppingCart[0].nIdRestaurante
        vc.selectedDiscountDelegate = self
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func actionOrder(_ sender:UIButton){}
    @IBAction func actionChangeAddress(_ sender:UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "addressController") as! MyDirectionsViewController
        vc.selectedAddress = self
        vc.defaultScreen = true
        self.present(vc, animated: true, completion: nil)
    }
}
extension ShoppingCartViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrShoppingCart!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "shoppingcarcell", for: indexPath) as! ShoppingCartAdapterTableViewCell
        
        cell.lblTitleITem.text = arrShoppingCart[indexPath.row].name
        cell.lblDescriptionITem.text = Utils.getEoticonsfromCategory(idCategory: arrShoppingCart[indexPath.row].category)
        cell.lblPRiceITem.text = String(arrShoppingCart[indexPath.row].price)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84.0
    }
}
extension ShoppingCartViewController:AddressSelectedDelegate{
    func selected(address: UserAddress) {
        lblAddresTitle.text = address.nombreDireccion
        lblAddresDescription.text = "\(address.nombreCalle ?? "") \(address.numeroExterior ?? ""), \(address.colonia ?? "")"
        btnOrder.isHidden = false
        heightDeliveryCost.constant = 26
    }
    
    func selectedToGo() {
        lblAddresTitle.text = "Recoger"
        lblAddresDescription.text = "Pide y recoge."
        heightDeliveryCost.constant = 0

    }
}

extension ShoppingCartViewController:DiscountSelectedDelegate{
    func selectedDiscount(selected: DiscountApplyModelRealm) {
        arrDiscount = realm.objects(DiscountApplyModelRealm.self).first
        calculateTotals()
    }
}
