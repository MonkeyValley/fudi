//
//  MyDirectionsViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 25/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import RealmSwift

class MyDirectionsViewController: UIViewController {

    @IBOutlet weak var lvMyDirections: UITableView!
    
    private let realm = try! Realm()
    var direccionesEntrega:Results<UserAddress>?
    let sharedPref = UserDefaults.standard
    var selectedAddress:AddressSelectedDelegate?
    var defaultScreen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        obtenerDirecciones()
    }

    func obtenerDirecciones() {
        direccionesEntrega = realm.objects(UserAddress.self)
        lvMyDirections.reloadData()
    }
    

    @IBAction func actionAddAddress(_ sender: UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "mapController") as! MapViewController
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func actionClose(_ sender:UIButton){
        self.dismiss(animated: true)
    }
}
extension MyDirectionsViewController:UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(defaultScreen){
            if(direccionesEntrega != nil){
                return direccionesEntrega!.count + 1
            }else{
                return 1
            }
        }else{
            if(direccionesEntrega != nil){
                return direccionesEntrega!.count
            }else{
                return 0
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "directioncell", for: indexPath) as! DirectionAdapterTableViewCell
        
        if(defaultScreen){
            if(indexPath.row == 0){
                cell.icDirectionCell.image = UIImage(systemName: "figure.walk")
                cell.lblDirectionNameCell.text = "Recoger"
                cell.lblDirectionLineCell.text = "Pasar por mi pedido  sucursal"
            }else{
                cell.icDirectionCell.image = UIImage(systemName: "location.fill")
                let item = direccionesEntrega![indexPath.row - 1]
                cell.lblDirectionNameCell.text = item.nombreDireccion
                cell.lblDirectionLineCell.text = "\(item.nombreCalle ?? "") \(item.numeroExterior ?? "")"
            }
        }else{
            cell.icDirectionCell.image = UIImage(systemName: "location.fill")
            let item = direccionesEntrega![indexPath.row]
            cell.lblDirectionNameCell.text = item.nombreDireccion
            cell.lblDirectionLineCell.text = "\(item.nombreCalle ?? "") \(item.numeroExterior ?? "")"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        var userDefaults = ""
        
        if(defaultScreen){
            if(indexPath.row == 0){
                
                sharedPref.removeObject(forKey: "_addressSelected")
                userDefaults = "{\"_id\":\"00000000\", \"nombreDireccion\":\"Recoger\", \"nombreCalle\":\"\", \"numeroExterior\":\"\" , \"numeroInterior\":\"\" , \"referencias\":\"\" , \"colonia\":\"\" , \"_lat\":\"\", \"_lng\":\"\" , \"Usuario\":\"\" , \"tipoDirecccion\":\"\" , \"regionEntrega\":\"\"}"
                if(selectedAddress != nil){
                    selectedAddress!.selectedToGo()
                }
                
            }else{
                let item = direccionesEntrega![indexPath.row - 1]
                userDefaults = "{\"_id\":\"\(item._id ?? "")\", \"nombreDireccion\":\"\(item.nombreDireccion ?? "")\", \"nombreCalle\":\"\(item.nombreCalle ?? "")\", \"numeroExterior\":\"\(item.numeroExterior ?? "")\" , \"numeroInterior\":\"\(item.numeroInterior ?? "")\" , \"referencias\":\"\(item.referencias ?? "")\" , \"colonia\":\"\(item.colonia ?? "")\" , \"_lat\":\(item._lat), \"_lng\":\(item._lng) , \"Usuario\":\"\(item.Usuario ?? "")\" , \"tipoDirecccion\":\(item.tipoDirecccion) , \"regionEntrega\":\"\(item.regionEntrega ?? "")\"}"
                if(selectedAddress != nil){
                    selectedAddress!.selected(address:item)
                }
            
            }
        }else{
            let item = direccionesEntrega![indexPath.row]
            userDefaults = "{\"_id\":\"\(item._id ?? "")\", \"nombreDireccion\":\"\(item.nombreDireccion ?? "")\", \"nombreCalle\":\"\(item.nombreCalle ?? "")\", \"numeroExterior\":\"\(item.numeroExterior ?? "")\" , \"numeroInterior\":\"\(item.numeroInterior ?? "")\" , \"referencias\":\"\(item.referencias ?? "")\" , \"colonia\":\"\(item.colonia ?? "")\" , \"_lat\":\(item._lat), \"_lng\":\(item._lng) , \"Usuario\":\"\(item.Usuario ?? "")\" , \"tipoDirecccion\":\(item.tipoDirecccion) , \"regionEntrega\":\"\(item.regionEntrega ?? "")\"}"
            if(selectedAddress != nil){
                selectedAddress!.selected(address:item)
            }
        }
        
        let jsonString = userDefaults
        let data: Data? = jsonString.data(using: .utf8)
        let json = (try? JSONSerialization.jsonObject(with: data!, options: [])) as? [String:AnyObject]
        self.sharedPref.set(json, forKey: "_addressSelected")
        self.sharedPref.synchronize()
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76.0
    }
}
protocol AddressSelectedDelegate {
    func selected(address:UserAddress)
    func selectedToGo()
}
