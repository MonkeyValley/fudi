//
//  AddDiscountViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 28/02/21.
//  Copyright © 2021 Monkey Valley. All rights reserved.
//

import UIKit

class AddDiscountViewController: UIViewController {

    
    @IBOutlet weak var txtCupon:UITextField!
    @IBOutlet weak var btnSendCupon:UIButton!
    @IBOutlet weak var btnBack:UIButton!
    
    let sharedPref = UserDefaults.standard
    var user:[String:Any]?
    var calert = CustoAlert()
    var onCupponDelegate:onCuponSuccess?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        user = sharedPref.object(forKey: "_userData") as? [String: Any]
    }
    
    @IBAction func actionAddCuppon(_ sender:UIButton){
        
        let button = UIButton()
        button.addTarget(self, action: #selector(self.actionErrorAlert), for: .touchUpInside)

        
        if(txtCupon.text != ""){
            let userID = user!["id"] as? String ?? ""
            let cupon = txtCupon.text!
            
            requestHelper.agregarCupon(userId: userID, cCodigoCupon: cupon) { (respuesta) in
                if(!respuesta!.bError){
                    let buttonOk = UIButton()
                    buttonOk.addTarget(self, action: #selector(self.actionOkAlert), for: .touchUpInside)

                    self.calert.showAlertInfo(title: respuesta!.data!.cCodigoCupon, description: respuesta!.data!.cDescripcion, buttonTextLeft: "", buttonTextRight: "Aceptar", type: "success", on: self, leftButton: nil, rightButton: buttonOk)
                }else{
                    self.calert.showAlertInfo(title: "Lo sentimos", description: respuesta!.cMensaje, buttonTextLeft: "", buttonTextRight: "Aceptar", type: "error", on: self, leftButton: nil, rightButton: button)
                }
            }
        }else{
            self.calert.showAlertInfo(title: "Espera.", description: "Todos los campos son requeridos.", buttonTextLeft: "", buttonTextRight: "Aceptar", type: "error", on: self, leftButton: nil, rightButton: button)
        }
    }

    @IBAction func actionExit(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func actionErrorAlert(){
        self.calert.dismissAlert()
    }
    
    @objc func actionOkAlert(){
        self.calert.dismissAlert()
        onCupponDelegate!.onClose(response: DiscountsModelRealm())
    }

}


