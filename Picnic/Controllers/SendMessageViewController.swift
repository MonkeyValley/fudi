//
//  SendMessageViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 25/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import RealmSwift

class SendMessageViewController: UIViewController {

    @IBOutlet weak var btnFoodOption: UIButton!
    @IBOutlet weak var btnServiceOption: UIButton!
    @IBOutlet weak var btnOtherOption: UIButton!
    @IBOutlet weak var txtAreaComment: UITextField!
    
    let sharedPref = UserDefaults.standard
    var alertCustom = CustoAlert()
    private let realm = try! Realm()
    var user:[String:AnyObject]?
    var arryParams:Array<DTO_Comentario> = Array()
    var addresSelected:[String:AnyObject]?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getInfoUser()
    }
    
    func getInfoUser() {
        user = sharedPref.value(forKey: "_userData") as? [String:AnyObject]
        addresSelected = sharedPref.object(forKey: "_addressSelected") as? [String: AnyObject]

    }

    @IBAction func actionCheckOpction(_ sender: UIButton) {
        
        if(sender.tag == 0){
            sender.setImage(#imageLiteral(resourceName: "ic_check"), for: .normal)
            sender.tag = 1
        }else{
            sender.setImage(#imageLiteral(resourceName: "ic_unchecked"), for: .normal)
            sender.tag = 0
        }
        
        switch sender {
        case btnFoodOption:
            let obj = DTO_Comentario()
            obj.descripcionComentario = "comida"
            obj.tipoComentario = 1
            
            arryParams.append(obj)
            break
        case btnServiceOption:
            let obj = DTO_Comentario()
            obj.descripcionComentario = "servicio de entrega"
            obj.tipoComentario = 2
            
            arryParams.append(obj)
            break
        case btnOtherOption:
            let obj = DTO_Comentario()
            obj.descripcionComentario = "otros"
            obj.tipoComentario = 3
            
            arryParams.append(obj)
            break
        default:
            break
        }
    }
    @IBAction func actionClose(_ sender:UIButton){
        self.dismiss(animated: true)
    }
    @IBAction func actionSendComments(_ sender: UIButton) {
        print("food \(btnFoodOption.tag)")
        print("service \(btnServiceOption.tag)")
        print("other \(btnOtherOption.tag)")
        let button = UIButton()
        button.addTarget(self, action: #selector(self.actionRight), for: .touchUpInside)
        
        
        if(txtAreaComment.text! != "" && arryParams.count > 0){
        
        requestHelper.guardarComentario(cTelefono:user!["phone"] as! String, cMail: user!["mail"] as! String, comentarios: txtAreaComment.text!, lstTiposComentario: arryParams) { result in
            
            if(!result!.bError){
                self.alertCustom.showAlertInfo(title:"Comentario enviado", description: "su comentario ha sido enviado, será revisado por nuestros colaboradores.", buttonTextLeft: "success",  buttonTextRight: "Ok", type: "", on: self, leftButton: nil, rightButton: button)
            }else{
                self.alertCustom.showAlertInfo(title:"¡Lo sentimos!", description: "Ha sucedido un error, intente mas tarde.", buttonTextLeft: "error",  buttonTextRight: "Ok", type: "error", on: self, leftButton: nil, rightButton: button)
            }
        }
        }else{
            self.alertCustom.showAlertInfo(title:"¡Espera!", description: "Faltaron algunos parametros.", buttonTextLeft: "error",  buttonTextRight: "Ok", type: "info", on: self, leftButton: nil, rightButton: button)
        }
    }

    @objc func actionRight(){
        
        alertCustom.dismissAlert()
        
    }
}
