//
//  LoginRegisterViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 22/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import RealmSwift


class LoginViewController: UIViewController {
    
    @IBOutlet weak var txtPhone:UITextField!
    @IBOutlet weak var txtPassword:UITextField!

//    Mark: Variables
    
    let sharedPref = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let actionClose = UITapGestureRecognizer(target: self, action: #selector(self.closeKeyboard))
        view.addGestureRecognizer(actionClose)
    }
    
    @objc func closeKeyboard(){
        view.endEditing(true)

    }

    @IBAction func actionLogin(_ sender: UIButton) {
        
        let phone = txtPhone.text!
        let pass = txtPassword.text!
        
        
        requestHelper.loginApp(cTelefono: phone, password: pass, playerId: Constantes.OPEN_PAY_SECRET_KEY) { (loginAppModel) in
            
            if(!loginAppModel!.bError){
                let storyboard = UIStoryboard(name: "Main", bundle: nil);
                let vc = storyboard.instantiateViewController(withIdentifier: "LoadDataController") as! LoadDataViewController
                vc.modalPresentationStyle = .fullScreen
                
                let userDefaults = "{\"userName\":\"\(loginAppModel!.data?.cNombres ?? "")\", \"id\":\"\(loginAppModel!.data?._id ?? "")\", \"mail\":\"\(loginAppModel!.data?.cMail ?? "")\", \"phone\":\"\(loginAppModel!.data?.cTelefono ?? "")\"}"
                
                let paymentsDefaults = "{\"id\":\"\(loginAppModel!.data?.openPayCredentials?.id ?? "")\", \"name\":\"\(loginAppModel!.data?.openPayCredentials?.name ?? "")\", \"last_name\":\"\(loginAppModel!.data?.openPayCredentials?.last_name ?? "")\", \"email\":\"\(loginAppModel!.data?.openPayCredentials?.email ?? "")\" , \"phone_number\":\"\(loginAppModel!.data?.openPayCredentials?.phone_number ?? "")\", \"address\":\"\(loginAppModel!.data?.openPayCredentials?.address ?? "")\", \"creation_date\":\"\(loginAppModel?.data?.openPayCredentials?.creation_date ?? "")\", \"external_id\":\"\(loginAppModel?.data?.openPayCredentials?.external_id ?? "")\",\"clabe\":\"\(loginAppModel?.data?.openPayCredentials?.clabe ?? "")\" }"
  
                
                let jsonString = userDefaults
                let data: Data? = jsonString.data(using: .utf8)
                let json = (try? JSONSerialization.jsonObject(with: data!, options: [])) as? [String:AnyObject]
                
                let jsonStringPayments = paymentsDefaults
                let dataPayments: Data? = jsonStringPayments.data(using: .utf8)
                let jsonPayments = (try? JSONSerialization.jsonObject(with: dataPayments!, options: [])) as? [String:AnyObject]
                
                self.sharedPref.set(json, forKey: "_userData")
                self.sharedPref.set(jsonPayments, forKey: "_paymentData")
                self.sharedPref.synchronize()
                
                let realm = try! Realm()
                print(Realm.Configuration.defaultConfiguration.fileURL!)
                
                
                try! realm.write {
                    realm.delete(realm.objects(UserAddress.self))
                }
                
                try! realm.write {
                    for i in loginAppModel!.data!.lstDirecciones! {
                        
                        let address = UserAddress()
                        
                        address._id = i._id
                        address.nombreDireccion = i.nombreDireccion
                        address.nombreCalle = i.nombreCalle
                        address.numeroExterior = i.numeroExterior
                        address.numeroInterior = i.numeroInterior
                        address.referencias = i.referencias
                        address.colonia = i.colonia
                        address._lat = i._lat
                        address._lng = i._lng
                        address.Usuario = i.Usuario
                        address.tipoDirecccion = i.tipoDirecccion
                        address.regionEntrega = i.regionEntrega
                    
                        realm.add(address)
                    }
                }
                self.present(vc, animated: true, completion: nil)
                
            
            }else{
                let alert = UIAlertController(title: "Usuario o contraseña incorrectos", message: "Verifique que los datos sean correctos e intente de nuevo.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}
