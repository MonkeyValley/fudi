//
//  PrivacyViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 25/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import WebKit

class PrivacyViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var webViewContainer: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: config.url_policy_privacy)!
        webViewContainer.navigationDelegate = self
        webViewContainer.load(URLRequest(url: url))
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("TERMINÓ")
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("FAIL")
    }
    
    @IBAction func actionClose(_ sender:UIButton){
        self.dismiss(animated: true)
    }
}
