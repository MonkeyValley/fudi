//
//  PayentMethodsListViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 24/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import RealmSwift


class PayentMethodsListViewController: UIViewController {

    @IBOutlet weak var lvPaymentMethod: UITableView!
    
    private let realm = try! Realm()
    private var arrayMEthodPay:Results<PaymentsModelRealm>?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getPayments()
    }
    
    func getPayments()  {
        
        arrayMEthodPay = self.realm.objects(PaymentsModelRealm.self)
        lvPaymentMethod.reloadData()
        
    }
    @IBAction func close(_ sender:UIButton){
        self.dismiss(animated: true)
    }

}

extension PayentMethodsListViewController: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMEthodPay!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "paymentmethodcell", for: indexPath) as! PaymentMethodAdapterTableViewCell
        
        switch arrayMEthodPay![indexPath.row].brand {
        case "visa":
            cell.imgTypePayentCell.image = #imageLiteral(resourceName: "ic_visa")
            break
        case "mastercart":
            cell.imgTypePayentCell.image = #imageLiteral(resourceName: "ic_amex")
            break
        case "american":
            cell.imgTypePayentCell.image = #imageLiteral(resourceName: "ic_mc")
            break
        default:
            break
        }
        
        
        cell.lblNameOwnerPaymentcCell.text = arrayMEthodPay![indexPath.row].holder_name
        cell.lblNumberCarCall.text = arrayMEthodPay![indexPath.row].card_number
        
        
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 61
    }
}
