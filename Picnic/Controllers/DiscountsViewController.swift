//
//  DiscountsViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 25/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import Lottie
import RealmSwift

class DiscountsViewController: UIViewController {

    @IBOutlet weak var tvDiscounts:UITableView!
    @IBOutlet weak var animView: AnimationView!
    
    let sharedPref = UserDefaults.standard
    private var animatView: AnimationView?
    private var discountsArray:Results<DiscountsModelRealm>?
    private var discountApply:DiscountApplyModelRealm?
    var calert = CustoAlert()
    private let realm = try! Realm()
    var nIdRestaurante = ""
    var selectedDiscountDelegate:DiscountSelectedDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        animatView = .init(name: "wait_man")
        animatView!.frame = animView.bounds
        animatView!.frame.size.width = view.frame.width - 40
        animatView!.loopMode = .loop
        animatView!.contentMode = .scaleAspectFill
        animView.addSubview(animatView!)
        animatView!.play()
        
        getCouponSelected()
        
        getDiscounts()
    }
    func getDiscounts()  {
        
        if(nIdRestaurante == ""){
            discountsArray = self.realm.objects(DiscountsModelRealm.self)
        }else{
            discountsArray = self.realm.objects(DiscountsModelRealm.self).filter("nIdRestaurante == '\(String(nIdRestaurante))'")
        }
        tvDiscounts.reloadData()
    }
    func getCouponSelected(){
        let getitem = realm.objects(DiscountApplyModelRealm.self)
        if(getitem != nil && getitem.count > 0){
            discountApply = getitem[0]
        }
    }
    @IBAction func addgoToCupon(_ sender:UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "adddiscountcontroller") as! AddDiscountViewController
        vc.onCupponDelegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func actionClose(_ sender:UIButton){
        self.dismiss(animated: true)
    }

    override func viewDidDisappear(_ animated: Bool) {
        if(selectedDiscountDelegate != nil){
            selectedDiscountDelegate?.selectedDiscount(selected: discountApply!)
        }
    }
}

extension DiscountsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discountsArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cuponcell", for: indexPath) as! CuponAdapterTableViewCell
        
        cell.lblTitleDiscount.text = discountsArray?[indexPath.row].cCodigoCupon ?? ""
        cell.lblDescriptionDiscount.text = discountsArray?[indexPath.row].cDescripcion ?? ""

        cell.btnAplyDiscount.tag = indexPath.row
        
        if(discountsArray![indexPath.row]._id == discountApply?._id ?? ""){
            
            cell.btnAplyDiscount.backgroundColor = #colorLiteral(red: 1, green: 0.6540061235, blue: 0.2667940259, alpha: 0.771822352)
            cell.btnAplyDiscount.setTitleColor( #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            
        }else{
            cell.btnAplyDiscount.backgroundColor = #colorLiteral(red: 0.9018717408, green: 0.9020231366, blue: 0.9018518329, alpha: 1)
            cell.btnAplyDiscount.setTitleColor (#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
        }
        if(discountsArray![indexPath.row].bCanjeado){
            cell.btnAplyDiscount.setTitle("Canjeado", for: .normal)
        }else{
            cell.btnAplyDiscount.setTitle("Aplicar", for: .normal)
            cell.btnAplyDiscount.addTarget(self, action: #selector(self.appliDiscount(_:)), for: .touchUpInside)
        }
        
        switch discountsArray![indexPath.row].nTipoCupon {
        case 1:
                cell.lblmountDiscount.text = "\(discountsArray?[indexPath.row].nValorCupon ?? 0)%"
            break
        case 2:
                cell.lblmountDiscount.text = "$\(discountsArray?[indexPath.row].nValorCupon ?? 0)"
            break
        case 3:
                cell.lblmountDiscount.text = "🛵💨"
            break
        case 4:
                cell.lblmountDiscount.text = "2x1"
            break
        case 5:
            cell.lblmountDiscount.text = "\(discountsArray?[indexPath.row].nValorCupon ?? 0)"
            break
        default:
            break
        }
        
        return cell
    }
    
    @objc func appliDiscount(_ sender:UIButton){
        
        let item = discountsArray![sender.tag]
        try! realm.write {
            
            self.realm.delete(self.realm.objects(DiscountApplyModelRealm.self))
           
            let obj = DiscountApplyModelRealm()
            obj._id = item._id
            obj.bCanjeado = item.bCanjeado
            obj.cCodigoCupon = item.cCodigoCupon
            obj.cDescripcion = item.cDescripcion
            obj.cNombreRestaurante = item.cNombreRestaurante
            obj.nIdCupon = item.nIdCupon
            obj.nIdRestaurante = item.nIdRestaurante
            obj.nIdUsuario = item.nIdUsuario
            obj.nTipoCupon = item.nTipoCupon
            obj.nValorCupon = item.nValorCupon
            
            realm.add(obj)

            
        }
        
        getCouponSelected()
        tvDiscounts.reloadData()
        let button = UIButton()
        button.addTarget(self, action: #selector(self.closeAlert), for: .touchUpInside)
        self.calert.showAlertInfo(title:"Cupón aplicado", description: "tu cupon \(item.cCodigoCupon) se aplicó, este cupón solo sera valido para \(item.cNombreRestaurante).", buttonTextLeft: "success",  buttonTextRight: "Ok", type: "", on: self, leftButton: nil, rightButton: button)
    }
    
    @objc func closeAlert(){
        self.calert.dismissAlert()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125.0
    }
}
protocol DiscountSelectedDelegate {
    func selectedDiscount(selected:DiscountApplyModelRealm)
}
extension DiscountsViewController:onCuponSuccess{
    
    func onClose(response:DiscountsModelRealm) {
        
        try! self.realm.write {
            self.realm.add(response)
        }
    }
}
