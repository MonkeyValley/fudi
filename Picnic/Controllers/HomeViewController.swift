//
//  HomeViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 22/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import RealmSwift


class HomeViewController: UIViewController {

    @IBOutlet weak var lvHome: UITableView!
    @IBOutlet weak var btnShowAddress: UIButton!
    @IBOutlet weak var tvLugares:UITableView!
    @IBOutlet weak var imgIconAddress:UIImageView!
    @IBOutlet weak var btnAddress:UIButton!


    var isHome = true
    var category = 0
    var lugaresArray:Results<sucursalModelReal>?
    var direccionesEntrega:Results<UserAddress>?
    
    let sharedPref = UserDefaults.standard
    private let realm = try! Realm()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) // get the Documents folder path
        let pathForDocumentDir = documentsPath[0]
        print("pathForDocumentDir: \(pathForDocumentDir)")
        
        let user = sharedPref.object(forKey: "_userData") as? [String: Any]
        
        var res = 0
        for i in 0...365 {
            res += i
        }
        
        print(res)
        
        if(user != nil){
            print(user!["mail"]!)
        }
        
        
        if(isHome){
            obtenerLugares()
        }else{
            obtenerLugaresPorCategoria()
        }
        
    }

    override func viewDidAppear(_ animated: Bool) {
        obtenerdirecciones()
    }
    func obtenerLugares(){
        lugaresArray = self.realm.objects(sucursalModelReal.self)
        tvLugares.reloadData()
    }
    
    func obtenerLugaresPorCategoria(){
        lugaresArray = self.realm.objects(sucursalModelReal.self).filter("nIdCategoria == %@", self.category)
        tvLugares.reloadData()
    }
    
    func obtenerdirecciones() {
        direccionesEntrega = realm.objects(UserAddress.self)
        
        let addresSelected = sharedPref.object(forKey: "_addressSelected") as? [String: Any]
        
        if(addresSelected != nil){
            
            let addresName = addresSelected!["nombreDireccion"] as? String ?? ""
            
            if(addresName == "" || addresName == "Recoger"){
                btnAddress.setTitle("Recoger  ", for: .normal)
                imgIconAddress.image = UIImage(named: "round_recoger")
            }else{
                btnAddress.setTitle("\(addresName)  ", for: .normal)
                imgIconAddress.image = UIImage(named: "round_entrega")
            }
            
        }else{
            if(direccionesEntrega!.count > 0 ){
                btnAddress.setTitle("\(direccionesEntrega![0].nombreDireccion!)  ", for: .normal)
                imgIconAddress.image = UIImage(named: "round_entrega")
            }else{
                btnAddress.setTitle("", for: .normal)
            }
        }
    }
    
    @IBAction func actionShowAddress(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "addressController") as! MyDirectionsViewController
        vc.selectedAddress = self
        vc.defaultScreen = true
        self.present(vc, animated: true, completion: nil)
    }
}
extension HomeViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(lugaresArray != nil){
            return lugaresArray!.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homecell", for: indexPath) as! HomeTableViewCell
        
        let item = lugaresArray![indexPath.row]
        cell.tituloLugar.text = item.cNombreRestaurante
        cell.sucursalLugar.text = item.cNombreSucursal
       
        cell.descripcionLugar.text = Utils.getEoticonsfromCategory(idCategory: item.nIdCategoria)
        cell.imgBgHomeCell.image = UIImage(named: Utils.getImagefromCategory(idCategory: item.nIdCategoria))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = lugaresArray![indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "placedetailsController") as! PlaceDatailsViewController
        vc.idPlace = item.nIdRestaurante!
        vc.namePlace = item.cNombreRestaurante!
        vc.subPlace = item.cNombreSucursal!
        vc.categoryId = item.nIdCategoria
        vc.idSubPlace = item._id!
        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250.0
    }
}

extension HomeViewController:AddressSelectedDelegate{
    func selectedToGo() {
        btnAddress.setTitle("Recoger  ", for: .normal)
        imgIconAddress.image = UIImage(named: "round_recoger")
        
    }
    
    func selected(address:UserAddress) {
        btnAddress.setTitle("\(address.nombreDireccion!)  ", for: .normal)
        imgIconAddress.image = UIImage(named: "round_entrega")
    }
}
