//
//  AddMethodPayViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 23/12/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit

class AddMethodPayViewController: UIViewController {

    @IBOutlet weak var bgTxtNameOwner: UIView!
    @IBOutlet weak var bgTxtNuberCard: UIView!
    @IBOutlet weak var bgDateExpCard: UIView!
    @IBOutlet weak var bgTxtCVVCard: UIView!
    @IBOutlet weak var txtCardNumber:UITextField!
    @IBOutlet weak var txtCardName:UITextField!
    @IBOutlet weak var txtCardDate:UITextField!
    @IBOutlet weak var txtCardCVV:UITextField!
    
    
    let prefShareds = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bgTxtCVVCard.backgroundColor = .secondarySystemBackground
        bgDateExpCard.backgroundColor = .secondarySystemBackground
        bgTxtNameOwner.backgroundColor = .secondarySystemBackground
        bgTxtNuberCard.backgroundColor = .secondarySystemBackground
    }

    
    func addCard(){
        
        let paymentinfo = prefShareds.object(forKey: "_paymentData") as? [String: Any]

        let cardNum = txtCardNumber.text!
        let cardNam = txtCardName.text!
        let cardDat = txtCardDate.text!
        let cardCVV = txtCardCVV.text!
        let idPayment = paymentinfo!["id"] as? String ?? ""
        
        requestHelper.addCardCustomer(openPayUserId: idPayment , card_number: cardNum, holder_name: cardNam, expiration_year: cardDat, expiration_month: cardDat, cvv2: cardCVV) { response in
   
            
        }
    }
    
    @IBAction func close(_ sender:UIButton){
        self.dismiss(animated: true)
    }
}
