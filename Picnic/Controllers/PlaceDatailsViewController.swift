//
//  PlaceDatailsViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 22/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import RealmSwift

class PlaceDatailsViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var svMenuSlide: UIScrollView!
    @IBOutlet weak var lvDetailsPlace: UITableView!
    @IBOutlet weak var lblEmptyState: UILabel!
    @IBOutlet weak var viewAddToCart: UIView!
    @IBOutlet weak var topContraint: NSLayoutConstraint!
    @IBOutlet weak var lblNumberItems: UILabel!
    @IBOutlet weak var imgPlaceDetail: UIImageView!
    @IBOutlet weak var lblNameRest:UILabel!
    @IBOutlet weak var lblNameSubRest:UILabel!
    @IBOutlet weak var lblTotalToPay:UILabel!

    var arrayCategories = [categoriaModel]()
    var arrayCategorySelected = [platilloModel]()
    var arrayStatus = [Bool]()
    var closeWindows:HandlerDelegate?
    
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    var pageNumber:CGFloat = 0.0
    var categorieSelected = 0
    let userdefaults = UserDefaults.standard
    var idPlace = ""
    var namePlace = ""
    var subPlace = ""
    var idSubPlace = ""
    var categoryId = 0
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        obtenerDetails()
        setImage()
        
        let item = realm.objects(CartModelRealm.self)
        if(item.count > 0){
            var totalnum = 0
            var totalToPay = 0.0
            item.forEach{ it in
                print(it._id)
                print(it.quantity)
                print(it.sizeProduct)

                totalToPay += it.price
                totalnum += it.quantity
            }
            lblTotalToPay.text = "$"+String(totalToPay)
            lblNumberItems.text = String(totalnum)
            self.viewAddToCart.frame.origin = CGPoint(x: 0, y: self.view.frame.height - 50)
            self.topContraint.constant = -50
        }else{
            lblNumberItems.text = "0"
            self.viewAddToCart.frame.origin = CGPoint(x: 0, y: self.view.frame.height )
            self.topContraint.constant = 0
        }
        

        
        
        
    }
    
    func setImage(){
        switch categoryId {
        case 0:
            imgPlaceDetail.image = UIImage(named: "cat_promo")
            break
        case 1:
            imgPlaceDetail.image = UIImage(named: "cat_sushi")
            break
        case 2:
            imgPlaceDetail.image = UIImage(named: "cat_hamburguesas")
            break
        case 3:
            imgPlaceDetail.image = UIImage(named: "cat_china")
            break
        case 4:
            imgPlaceDetail.image = UIImage(named: "cat_tacos")
            break
        case 5:
            imgPlaceDetail.image = UIImage(named: "cat_mariscos")
            break
        case 6:
            imgPlaceDetail.image = UIImage(named: "cat_pizza")
            break
        case 7:
            imgPlaceDetail.image = UIImage(named: "cat_mexicana")
            break
        case 8:
            imgPlaceDetail.image = UIImage(named: "cat_cortes")
            break
        case 9:
            imgPlaceDetail.image = UIImage(named: "cat_ensalada")
            break
        case 10:
            imgPlaceDetail.image = UIImage(named: "cat_pasteles")
            break
        default:
            break
        }
    }
    
    func obtenerDetails()  {
        
        lblNameRest.text = namePlace
        lblNameSubRest.text = subPlace
        

            requestHelper.getCargaInicialRestaurante(nIdRestaurante:idPlace) { (response) in
                self.arrayCategories = response!.data!.lstCategorias!
                self.arrayCategorySelected = response!.data!.lstCategorias![0].lstPlatillos!
                
                for _ in 0...response!.data!.lstCategorias!.count{
                    self.arrayStatus.append(false)
                }
                
                self.arrayStatus[0] = true
                self.initenu()
                self.lvDetailsPlace.reloadData()
            }
        
        
    }

    func initenu(){
        
        if(arrayCategories.count == 0){
            lvDetailsPlace.isHidden = true
            lblEmptyState.isHidden = false
        }else{
            lvDetailsPlace.isHidden = false
            lblEmptyState.isHidden = true
        }
        let widthitem = self.view.frame.size.width / 3.5
        let subViews = self.svMenuSlide.subviews
        for subview in subViews{ subview.removeFromSuperview()}
        
        svMenuSlide.contentSize = CGSize(width: widthitem * CGFloat(arrayCategories.count), height:self.svMenuSlide.frame.size.height)
        svMenuSlide.delegate = self
        
        for itme in 0..<arrayCategories.count {
            
            frame.origin.x = (widthitem) * CGFloat(itme)
            frame.size = CGSize(width: widthitem , height: self.svMenuSlide.frame.size.height)

            let conteiner = UIView(frame: frame)
            let gesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
            conteiner.addGestureRecognizer(gesture)
            conteiner.tag = itme
            let label = UILabel()
            label.textColor = UIColor.darkGray
            label.text = arrayCategories[itme].cDescripcion
            label.numberOfLines = 0
            label.frame.size = CGSize(width: conteiner.frame.width, height: conteiner.frame.height)
            label.frame.origin.x = 0
            label.frame.origin.y = 0
            label.font = UIFont(name: "HelveticaNeue-Bold", size: 12.0)
            label.textAlignment = .center
            
            if(arrayStatus[itme]){
                let indicator = UIView(frame: CGRect(x: 0, y: conteiner.frame.height - 2, width: conteiner.frame.width, height: 2))
                indicator.backgroundColor = UIColor(red: 254/255, green: 88/255, blue: 34/255, alpha: 1.0)
                conteiner.addSubview(indicator)
            }
            
            conteiner.addSubview(label)
            self.svMenuSlide.addSubview(conteiner)
            
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        let view = sender.view!
        let tag = view.tag

        
        for i in 0...arrayCategories.count-1 {arrayStatus[i] = false}
           //self.svenuSlider.contentOffset.x = view.frame.origin.x
        self.categorieSelected = tag
        self.arrayStatus[tag] = true
        self.arrayCategorySelected = arrayCategories[tag].lstPlatillos!
        self.initenu()
        self.lvDetailsPlace.reloadData()
       }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        _ = svMenuSlide.contentOffset.x / (self.view.frame.size.width/3.5)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageNumber = svMenuSlide.contentOffset.x / (self.view.frame.size.width/3.5)
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        if closeWindows != nil {
            closeWindows!.closeWindow()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionPay(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "shoppingcarcontroller") as! ShoppingCartViewController
        self.present(vc, animated: true, completion: nil);
        
    }
    @IBAction func actionDeleteShoppingCart(_ sender:UIButton){
        do{
            try realm.write{
                realm.delete(realm.objects(CartModelRealm.self))
            }
        }catch{
            let alert = UIAlertController(title: "Error al eliminar", message: "Por favor intente de nuevo.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        lblNumberItems.text = "0"
        self.viewAddToCart.frame.origin.y = self.viewAddToCart.frame.origin.y + 50
        self.topContraint.constant = 0
    
    }
}
extension PlaceDatailsViewController:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCategorySelected.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "placedetailscell", for: indexPath) as! PlaceDetailsTableViewCell
        
        cell.lblTitleCell.text = arrayCategorySelected[indexPath.row].cNombrePlatillo
        cell.lblDescriptionCell.text = arrayCategorySelected[indexPath.row].cDescripcion
        cell.lblPriceCell.text = "$\(arrayCategorySelected[indexPath.row].lstTamanios![0].nPrecio!)"
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let itemSelected = arrayCategorySelected[indexPath.row]
        itemSelected.nIdRestaurante = idPlace
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "productdetailcontroller") as! ProductDetailsViewController
        vc.addtocart = self
        vc.category = categoryId
        vc.data = itemSelected
        vc.nIdSucursal = idSubPlace
        self.present(vc, animated: true, completion: nil);
    }
    
}
extension PlaceDatailsViewController: AddtoCartDelegate{
    func error() {}
    
    func add() {
        print("ADD TO CART")
        
        //let fetchReq:NSFetchRequest<ShoppingCart> = ShoppingCart.fetchRequest()
        let fetchReq = realm.objects(CartModelRealm.self)
        if(fetchReq.count > 0){
            var totalnum = 0
            var totalToPay = 0.0
            fetchReq.forEach{ it in
                totalnum += it.quantity
                totalToPay += it.price
                print(it._id)
                print(it.quantity)
                print(it.sizeProduct!)
            }
            
            lblNumberItems.text = String(totalnum)
            lblTotalToPay.text = "$"+String(totalToPay)
            
        }else{
            lblNumberItems.text = "0"
        }
        
        UIView.animate(withDuration: 0.5, delay: 0.4, animations: {
            self.viewAddToCart.frame.origin.y = self.view.frame.height - 50
                        
        }, completion: {(value: Bool) in
            self.viewAddToCart.frame.origin = CGPoint(x: 0, y: self.view.frame.height - 50)
            self.topContraint.constant = -50
        })
        
    }
    
}

