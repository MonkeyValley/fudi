//
//  RestauarantSuggestionViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 26/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import RealmSwift


class RestauarantSuggestionViewController: UIViewController {

    @IBOutlet weak var txtNameRestaurant:UITextField!
    @IBOutlet weak var txtPhoneRestaurant:UITextField!

    let sharedPref = UserDefaults.standard
    private let realm = try! Realm()
    
    var user:[String:Any]?
    var addresSelectedG:[String:Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        user = sharedPref.object(forKey: "_userData") as? [String: Any]
        obtenerdirecciones()
        
    }
    
    func obtenerdirecciones() {
        let direccionesEntrega = realm.objects(UserAddress.self)
        let addresSelected = sharedPref.object(forKey: "_addressSelected") as? [String: Any]

        
        if(addresSelected != nil){
            let addresName = addresSelected!["nombreDireccion"] as? String ?? ""
            
            if(addresName == "" || addresName == "Recoger"){
                let obj = direccionesEntrega[0]
                addresSelectedG = ["_id": obj._id ?? "", "regionEntrega":obj.regionEntrega ?? ""]
                
            }else{
                addresSelectedG = ["_id": addresSelected!["_id"] as? String ?? "" , "regionEntrega":addresSelected!["regionEntrega"] as? String ?? ""]
            }
            
        }else{
            if(direccionesEntrega.count > 0 ){
                let obj = direccionesEntrega[0]
                addresSelectedG = ["_id": obj._id ?? "", "regionEntrega":obj.regionEntrega ?? ""]
            }else{
                addresSelectedG = ["_id": "", "regionEntrega":""]
            }
        }
    }

    @IBAction func sendSugestion(_ sender:UIButton?){
     
        let nameRestaurant = txtNameRestaurant.text!
        let phoneRestaurant = txtPhoneRestaurant.text!
        let userName = user!["userName"] as? String ?? ""
        let userPhone = user!["phone"] as? String ?? ""
        let idAddres = addresSelectedG!["_id"] as? String ?? ""
        let idUser = user!["id"] as? String ?? ""
        let deliveryRegion = addresSelectedG!["regionEntrega"] as? String ?? ""
        
        print("nameRestaurant - \(nameRestaurant)")
        print("phoneRestaurant - \(phoneRestaurant)")
        print("userName - \(userName)")
        print("userPhone - \(userPhone)")
        print("idAddres - \(idAddres)")
        print("idUser - \(idUser)")
        print("deliveryRegion - \(deliveryRegion)")
        print("---------------------------------------------")
        
        requestHelper.guardarSugerencia(cNombreCliente: userName, cNombreRestaurante: nameRestaurant , cTelefonoCliente: userPhone, cTelefonoSucursal: phoneRestaurant, nIdDireccion: idAddres, nIdUsuario: idUser, regionEntrega: deliveryRegion) { response in
            
            if(!response!.bError){
                let alert = UIAlertController(title: "¡Excelente!", message: "Muchas gracias por tu ayuda, evaluaremos tu petición para poder tener tus restaurantes favoritos.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {_ in 
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
                let alert = UIAlertController(title: "¡UPS!", message: "No se pudo enviar la sugerencia intente mas tarde.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }
            
        }
    }
    @IBAction func actionClose(_ sender:UIButton){
        self.dismiss(animated: true)
    }

}
