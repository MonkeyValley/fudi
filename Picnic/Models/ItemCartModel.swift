//
//  ItemCartModel.swift
//  Picnic
//
//  Created by InnovacionVO on 06/11/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation

class itemcart {
    
    var id:Int
    var name:String
    var description:String
    var category:String
    var price:Double
    var number:Int
    
    init(id:Int, name:String, description:String, category:String, price:Double, number:Int) {
        self.category = category
        self.description = description
        self.id = id
        self.name = name
        self.number = number
        self.price = price
    }
}
