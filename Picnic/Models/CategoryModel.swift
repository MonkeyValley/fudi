//
//  CategoryModel.swift
//  Picnic
//
//  Created by InnovacionVO on 06/11/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation
class Category{
    var id:Int
    var title:String
    var status:Bool
    
    init(id:Int, title:String, status:Bool) {
        self.id = id
        self.title = title
        self.status = status
    }
}
