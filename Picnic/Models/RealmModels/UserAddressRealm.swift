//
//  UserAddress.swift
//  Picnic
//
//  Created by InnovacionVO on 13/12/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//


import Foundation
import RealmSwift

class UserAddress: Object {
    
    /*init(_id:String, nombreDireccion:String, nombreCalle:String, numeroExterior:String, numeroInterior:String, referencias:String, colonia:String, _lat:Double, _lng:Double, Usuario:String, tipoDirecccion:Int, regionEntrega:String) {
        self._id = _id
        self.nombreDireccion = nombreDireccion
        self.nombreCalle = nombreCalle
        self.numeroExterior = numeroExterior
        self.numeroInterior = numeroInterior
        self.referencias = referencias
        self.colonia = colonia
        self._lat = _lat
        self._lng = _lng
        self.Usuario = _id
        self.tipoDirecccion = tipoDirecccion
        self.regionEntrega = regionEntrega
    }
    */
    @objc dynamic var _id:String?
    @objc dynamic var nombreDireccion:String?
    @objc dynamic var nombreCalle:String?
    @objc dynamic var numeroExterior:String?
    @objc dynamic var numeroInterior:String?
    @objc dynamic var referencias:String?
    @objc dynamic var colonia:String?
    @objc dynamic var _lat:Double = 0.0
    @objc dynamic var _lng:Double = 0.0
    @objc dynamic var Usuario:String?
    @objc dynamic var tipoDirecccion:Int = 0
    @objc dynamic var regionEntrega:String?
    
}
