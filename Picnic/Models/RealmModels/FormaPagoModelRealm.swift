//
//  FormaPagoModelRealm.swift
//  Picnic
//
//  Created by InnovacionVO on 21/12/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation
import RealmSwift

class FormaPagoModelRealm: Object {
    
    @objc dynamic var device_session_id:String?
    @objc dynamic var idTarjeta:String?
    @objc dynamic var nTipoPago:Int = 0
    @objc dynamic var pagaCon:Int = 0
}
