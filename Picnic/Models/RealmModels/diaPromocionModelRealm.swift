//
//  diaPromocionModel.swift
//  Picnic
//
//  Created by InnovacionVO on 13/12/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation
import RealmSwift

class diaPromocionModelRealm: Object {
    @objc dynamic var bActivo: Bool = false
    @objc dynamic var dia: String?
    @objc dynamic var numeroDia: Int = 0
    @objc dynamic var sucursalId: String = ""
}
