//
//  DiscountApplyModelRealm.swift
//  Picnic
//
//  Created by InnovacionVO on 25/03/21.
//  Copyright © 2021 Monkey Valley. All rights reserved.
//

import Foundation
import RealmSwift

class DiscountApplyModelRealm: Object {
    
    @objc dynamic var _id: String = ""
    @objc dynamic var nIdUsuario: String = ""
    @objc dynamic var nIdCupon: String = ""
    @objc dynamic var bCanjeado: Bool = false
    @objc dynamic var cCodigoCupon: String = ""
    @objc dynamic var cDescripcion: String = ""
    @objc dynamic var cNombreRestaurante: String = ""
    @objc dynamic var nIdRestaurante: String = ""
    @objc dynamic var nTipoCupon: Int = 0
    @objc dynamic var nValorCupon: Int = 0
    
    
}
