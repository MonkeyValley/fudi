//
//  PlatilloModelRealm.swift
//  Picnic
//
//  Created by InnovacionVO on 23/03/21.
//  Copyright © 2021 Monkey Valley. All rights reserved.
//

import Foundation
import RealmSwift


class PlatilloModelRealm: Object {
    @objc dynamic var orderId: String = ""
    @objc dynamic var _id: String = ""
    @objc dynamic var bFoto: Bool = false
    @objc dynamic var bIngredienteElegir: Bool  = false
    @objc dynamic var bPlatilloHome: Bool  = false
    @objc dynamic var cDescripcion: String = ""
    @objc dynamic var cFoto: String = ""
    @objc dynamic var cNombrePlatillo: String = ""
    @objc dynamic var comentarios: String = ""
    @objc dynamic var nIdCategoria: String = ""
    @objc dynamic var nIdRestaurante: String = ""
    @objc dynamic var nIngredientes: Int  = 0
    @objc dynamic var nPrecio: Int = 0
    @objc dynamic var quantity: Int = 0
}
