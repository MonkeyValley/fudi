//
//  getSucursalesModel.swift
//  Picnic
//
//  Created by InnovacionVO on 13/12/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation
import RealmSwift

class sucursalModelReal: Object {
    @objc dynamic var _id: String?
    @objc dynamic var bActivo: Bool = false
    @objc dynamic var cPassword: String?
    @objc dynamic var cLogin: String?
    @objc dynamic var dHoraCierre: String?
    @objc dynamic var dHoraApertura: String?
    @objc dynamic var cDireccion: String?
    @objc dynamic var nTiempoEntrega: Int = 0
    @objc dynamic var costoEnvio: Int = 0
    @objc dynamic var cNombreSucursal: String?
    @objc dynamic var nLatitud: Double = 0.0
    @objc dynamic var nLongitud: Double = 0.0
    @objc dynamic var nCostoEntrega: Int = 0
    @objc dynamic var nTiempoEspera: Int = 0
    @objc dynamic var nIdRestaurante: String?
    @objc dynamic var TIME_ZONE: String?
    @objc dynamic var nHoraApertura: Int = 0
    @objc dynamic var nHoraCierre: Int = 0
    @objc dynamic var nMinutoApertura: Int = 0
    @objc dynamic var nMinutoCierre: Int = 0
    @objc dynamic var regionEntrega: String?
    @objc dynamic var bSoloRecoger: Bool = false
    @objc dynamic var cTelefono: String?
    @objc dynamic var tipoEntrega: Int = 0
    @objc dynamic var cNombreRestaurante: String?
    @objc dynamic var nIdCategoria: Int = 0
    @objc dynamic var cFoto: String?
    @objc dynamic var nIdFormaPago: Int = 0
    @objc dynamic var pedidoMinimo: Int = 0
    @objc dynamic var nTipoDescuento: Int = 0
    @objc dynamic var descuentoPicnic: Int = 0
    
    var lstRangoEntrega:[locationModelRealm] = []
    var diasDescuentoPicnic:[diaPromocionModelRealm] = []
    
    /*convenience init(_id:String, bActivo:Bool, cPassword:String, cLogin:String, dHoraCierre:String, dHoraApertura:String, cDireccion:String, nTiempoEntrega:Int, costoEnvio:Int, cNombreSucursal:String,  nLatitud:Double, nLongitud:Double, nCostoEntrega:Int,nTiempoEspera:Int, nIdRestaurante:String, TIME_ZONE:String, nHoraApertura:Int, nHoraCierre:Int, nMinutoApertura:Int, nMinutoCierre:Int, regionEntrega:String, bSoloRecoger:Bool, cTelefonoc:String, tipoEntrega:Int, cNombreRestaurante:String, nIdCategoria:Int, cFoto:String, nIdFormaPago:Int, pedidoMinimo:Int, nTipoDescuento:Int, descuentoPicnic:Int, lstRangoEntrega:[locationModelRealm],  diasDescuentoPicnic:[diaPromocionModelRealm]){
        self.init()
        self._id = _id
        self.bActivo = bActivo
        self.cPassword = cPassword
        self.cLogin = cLogin
        self.dHoraCierre = dHoraCierre
        self.dHoraApertura = dHoraApertura
        self.cDireccion = cDireccion
        self.nTiempoEntrega = nTiempoEntrega
        self.costoEnvio  = costoEnvio
        self.cNombreSucursal = cNombreSucursal
        self.nLatitud = nLatitud
        self.nLongitud = nLongitud
        self.nCostoEntrega = nCostoEntrega
        self.nTiempoEspera = nTiempoEspera
        self.nIdRestaurante = nIdRestaurante
        self.TIME_ZONE = TIME_ZONE
        self.nHoraApertura = nHoraApertura
        self.nHoraCierre = nHoraCierre
        self.nMinutoApertura = nMinutoApertura
        self.nMinutoCierre = nMinutoCierre
        self.regionEntrega = regionEntrega
        self.bSoloRecoger = bSoloRecoger
        self.cTelefono = cTelefonoc
        self.tipoEntrega = tipoEntrega
        self.cNombreRestaurante = cNombreRestaurante
        self.nIdCategoria = nIdCategoria
        self.cFoto = cFoto
        self.nIdFormaPago = nIdFormaPago
        self.pedidoMinimo = pedidoMinimo
        self.nTipoDescuento = nTipoDescuento
        self.descuentoPicnic = descuentoPicnic
        
        
        self.lstRangoEntrega = lstRangoEntrega
        self.diasDescuentoPicnic = diasDescuentoPicnic
    }*/
    
}
