//
//  OrdersModelRealm.swift
//  Picnic
//
//  Created by InnovacionVO on 21/12/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation
import RealmSwift

class OrdersModelRealm: Object {
    @objc dynamic var _id: String?
    @objc dynamic var bPedidoGuardado:Bool = false
    @objc dynamic var bCompraPicnic:Bool = false
    @objc dynamic var bPedidoInvitado:Bool = false
//    @objc dynamic var dataOpenPay:String? //-----------------------
    @objc dynamic var fechaPedido:String?
    @objc dynamic var grupo:String?
    @objc dynamic var nAnioPedido:Int = 0
    @objc dynamic var nDiaPedido:Int = 0
    @objc dynamic var nDiaSemanaPedido:Int = 0
    @objc dynamic var nFechaPedido:Int = 0
    @objc dynamic var nIdCategoria:Int = 0
    @objc dynamic var nIdRestaurante:String?
    @objc dynamic var nIdSucursal:String?
    @objc dynamic var nIdUsuario:String?
    @objc dynamic var nMesPedido:Int = 0
    @objc dynamic var nSemanaPedido:Int = 0
    @objc dynamic var pedido:PedidoModelRealm?
    @objc dynamic var regionEntrega:String?
    @objc dynamic var status:Int = 0
    @objc dynamic var tipoCompra:Int = 0
}
