//
//  CategoryModelRealm.swift
//  Picnic
//
//  Created by InnovacionVO on 23/12/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation
import RealmSwift

class CategoryModelRealm: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String?
    @objc dynamic var count: Int = 0
}
