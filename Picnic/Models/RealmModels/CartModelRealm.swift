//
//  CartModelRealm.swift
//  Picnic
//
//  Created by InnovacionVO on 04/01/21.
//  Copyright © 2021 Monkey Valley. All rights reserved.
//

import Foundation
import RealmSwift
class CartModelRealm: Object {
    
    @objc dynamic var _id: Int = 0
    @objc dynamic var name: String?
    @objc dynamic var quantity: Int = 0
    @objc dynamic var note: String?
    @objc dynamic var sizeProduct: String?
    @objc dynamic var price: Double = 0.0
    @objc dynamic var idSize:String?
    @objc dynamic var idProduct: String?
    @objc dynamic var category: Int = 0
    @objc dynamic var nIdRestaurante: String = ""
    @objc dynamic var nIdSucursal: String = ""
}
