//
//  locationModelRealm.swift
//  Picnic
//
//  Created by InnovacionVO on 13/12/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation
import RealmSwift

class locationModelRealm: Object {
    @objc dynamic var  lat: Double = 0.0
    @objc dynamic var lng: Double = 0.0
}
