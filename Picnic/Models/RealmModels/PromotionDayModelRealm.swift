//
//  PromotionDayModelRealm.swift
//  Picnic
//
//  Created by InnovacionVO on 23/12/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation
import RealmSwift

class PromotionDayModelRealm: Object {
    @objc dynamic var dia:String?
    @objc dynamic var bActivo:Bool = false
    @objc dynamic var numeroDia:Int = 0
}
