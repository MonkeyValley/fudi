//
//  PromosModelRealm.swift
//  Picnic
//
//  Created by InnovacionVO on 23/12/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation
import RealmSwift

class PromosModelRealm: Object {
    
    @objc dynamic var _id:String?
    @objc dynamic var bAccionBanner:Bool = false
    @objc dynamic var minutoFin:Int = 0
    @objc dynamic var minutoInicio:Int = 0
    @objc dynamic var horaFin:Int = 0
    @objc dynamic var horaInicio:Int = 0
    @objc dynamic var bRestriccionHora:Bool = false
    @objc dynamic var bRestriccionDia:Bool = false
    @objc dynamic var nPrecio:Int = 0
    @objc dynamic var cDescripcion:String?
    @objc dynamic var cNombrePromocion:String?
    @objc dynamic var cFoto:String?
    @objc dynamic var nIdRestaurante:String?
    @objc dynamic var dHoraApertura:String?
    @objc dynamic var dHoraCierre:String?
    @objc dynamic var nombreRestaurante:String?
    @objc dynamic var nIdCategoria:Int = 0
    
    func dynamicList(_ promotionday: List<PromotionDayModelRealm>) -> List<PromotionDayModelRealm> {
        return promotionday
    }
    
}
