//
//  PaymentsModelRealm.swift
//  Picnic
//
//  Created by InnovacionVO on 24/01/21.
//  Copyright © 2021 Monkey Valley. All rights reserved.
//

import Foundation
import RealmSwift
class PaymentsModelRealm: Object {
    
    @objc dynamic var id: String = ""
    @objc dynamic var type: String = ""
    @objc dynamic var brand: String = ""
    @objc dynamic var address: String = ""
    @objc dynamic var card_number: String = ""
    @objc dynamic var holder_name: String = ""
    @objc dynamic var expiration_year: String = ""
    @objc dynamic var expiration_month: String = ""
    @objc dynamic var allows_charges: Bool = false
    @objc dynamic var allows_payouts: Bool = false
    @objc dynamic var creation_date: String = ""
    @objc dynamic var customer_id: String = ""
    @objc dynamic var bank_name: String = ""
    @objc dynamic var points_type: String = ""
    @objc dynamic var bank_code: String = ""
    @objc dynamic var points_card: Bool = false
    
}
