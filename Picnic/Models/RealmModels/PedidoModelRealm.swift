//
//  PedidoModelRealm.swift
//  Picnic
//
//  Created by InnovacionVO on 21/12/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation
import RealmSwift

class PedidoModelRealm: Object {
    @objc dynamic var bCompraInvitado = false
    @objc dynamic var bRecoger = false
    @objc dynamic var costoEnvio = 0
    @objc dynamic var fechaPedido:String?
    @objc dynamic var formaDePago:FormaPagoModelRealm?
    @objc dynamic var index = 0
    @objc dynamic var nIdCategoria = 0
    @objc dynamic var nIdCupon:String?
    @objc dynamic var nIdRestaurante:String?
    @objc dynamic var nIdSucursal:String?
    @objc dynamic var nIdUsuario:String?
    @objc dynamic var nombreRestaurante:String?
    @objc dynamic var nombreSucursal:String?
    @objc dynamic var playerId:String?
    @objc dynamic var regionEtrega:String?
    @objc dynamic var statusPedido = 0
    @objc dynamic var subTotal = 0.0
    @objc dynamic var tipoCompra = 0
    @objc dynamic var total = 0.0
    @objc dynamic var descuentoPicnic = 0
}
