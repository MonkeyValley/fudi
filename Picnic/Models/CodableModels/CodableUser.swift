//
//  CodableUser.swift
//  Picnic
//
//  Created by InnovacionVO on 30/12/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation

class CodableUser {
    
    struct User: Codable {
        let id: String?
        let mail: String?
        let phone: Int
        let userName: String?

        private enum CodingKeys: String, CodingKey {
            case id = "user_id"
            case mail = "user_mail"
            case phone = "user_phone"
            case userName = "user_userName"
        }
    }
}

