//
//  ShoppingCart+CoreDataProperties.swift
//  
//
//  Created by InnovacionVO on 06/11/20.
//
//

import Foundation
import CoreData


extension ShoppingCart {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ShoppingCart> {
        return NSFetchRequest<ShoppingCart>(entityName: "ShoppingCart")
    }

    @NSManaged public var category: String?
    @NSManaged public var id: Int32
    @NSManaged public var name: String?
    @NSManaged public var number: Int16
    @NSManaged public var price: String?

}
